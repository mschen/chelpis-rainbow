#include "mbedtls_sha256.h"
#include "rainbow-deps-api.h"

// the wrapped API entry point
void sha256(uint8_t *dgst, const uint8_t *msg, unsigned msglen) {
    int errcode = mbedtls_sha256_ret(msg, msglen, dgst, 0);
    (void) errcode;
}
