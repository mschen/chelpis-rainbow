#include "rainbow_16.h"
#include "blas.h"
#include "hash_utils.h"
#include "mpkc.h"
#include "rainbow_config.h"

#include <string.h>

void rainbow_pubmap(uint8_t *z, const uint8_t *poly, const uint8_t *w) {
    gf16mpkc_mq_eval_n_m(z, poly, w, _PUB_N, _PUB_M);
}

#ifndef _DEBUG_RAINBOW_

static unsigned rainbow_ivs_central_map(uint8_t *r, const rainbow_ckey *k, const uint8_t *a);

static void rainbow_central_map(uint8_t *r, const rainbow_ckey *k, const uint8_t *a);

static void rainbow_pubmap_seckey(uint8_t *z, const rainbow_key *sk, const uint8_t *w);

#endif

#ifndef _DEBUG_RAINBOW_
static
#endif
        void
        rainbow_pubmap_seckey(uint8_t *z, const rainbow_key *sk, const uint8_t *w) {

    uint8_t tt[_PUB_N_BYTE] = {0};
    uint8_t tt2[_PUB_N_BYTE] = {0};

    gf16mat_prod(tt, sk->mat_t, _PUB_N_BYTE, _PUB_N, w);
    gf256v_add(tt, sk->vec_t, _PUB_N_BYTE);

    rainbow_central_map(tt2, &sk->ckey, tt);

    gf16mat_prod(z, sk->mat_s, _PUB_M_BYTE, _PUB_M, tt2);
    gf256v_add(z, sk->vec_s, _PUB_M_BYTE);
}

/////////////////////////////

static inline void gen_l1_mat(uint8_t *mat, const rainbow_ckey *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O1; i++) {
        gf16mat_prod(mat + i * _O1_BYTE, k->l1_vo[i], _O1_BYTE, _V1, v);
        gf256v_add(mat + i * _O1_BYTE, k->l1_o + i * _O1_BYTE, _O1_BYTE);
    }
}

static inline void gen_l2_mat(uint8_t *mat, const rainbow_ckey *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O2; i++) {
        gf16mat_prod(mat + i * _O2_BYTE, k->l2_vo[i], _O2_BYTE, _V2, v);
        gf256v_add(mat + i * _O2_BYTE, k->l2_o + i * _O2_BYTE, _O2_BYTE);
    }
}

#ifndef _DEBUG_RAINBOW_
static
#endif
        void
        rainbow_central_map(uint8_t *r, const rainbow_ckey *k, const uint8_t *a) {
#ifdef _DEBUG_MPKC_
    memcpy(r, a + _V1_BYTE, _PUB_M_BYTE);
    return;
#endif
    uint8_t mat1[_O2 * _O2];
    uint8_t temp[_O2_BYTE];

    gen_l1_mat(mat1, k, a);

    gf16rowmat_prod(r, mat1, _O1, _O1_BYTE, a + _V1_BYTE);
    gf16mpkc_mq_eval_n_m(temp, k->l1_vv, a, _V1, _O1);
    gf256v_add(r, temp, _O1_BYTE);

    gen_l2_mat(mat1, k, a);

    gf16rowmat_prod(r + _O1_BYTE, mat1, _O2, _O2_BYTE, a + _V2_BYTE);
    gf16mpkc_mq_eval_n_m(temp, k->l2_vv, a, _V2, _O2);
    gf256v_add(r + _O1_BYTE, temp, _O2_BYTE);
}

#ifndef _DEBUG_RAINBOW_
static
#endif
        unsigned
        rainbow_ivs_central_map(uint8_t *r, const rainbow_ckey *k, const uint8_t *a) {
#ifdef _DEBUG_MPKC_
    memcpy(r + _V1_BYTE, a, _PUB_M_BYTE);
    return 1;
#endif
    uint8_t mat1[_O1 * _O1];
    uint8_t temp[_O1_BYTE];
    gf16mpkc_mq_eval_n_m(temp, k->l1_vv, r, _V1, _O1);
    gf256v_add(temp, a, _O1_BYTE);
    gen_l1_mat(mat1, k, r);
    unsigned r1 = gf16mat_solve_linear_eq(r + _V1_BYTE, mat1, temp, _O1);

    uint8_t mat2[_O2 * _O2];
    uint8_t temp2[_O2_BYTE];
    gen_l2_mat(mat2, k, r);
    gf16mpkc_mq_eval_n_m(temp2, k->l2_vv, r, _V2, _O2);
    gf256v_add(temp2, a + _O1_BYTE, _O2_BYTE);
    unsigned r2 = gf16mat_solve_linear_eq(r + _V2_BYTE, mat2, temp2, _O2);

    return r1 & r2;
}

/// algorithm 8
int rainbow_verify(const uint8_t *digest, const uint8_t *signature, const uint8_t *pk) {
    uint8_t digest_ck[_PUB_M_BYTE];
    rainbow_pubmap(digest_ck, pk, signature);

    uint8_t correct[_PUB_M_BYTE];
    uint8_t digest_salt[_HASH_LEN + _SALT_BYTE];
    memcpy(digest_salt, digest, _HASH_LEN);
    memcpy(digest_salt + _HASH_LEN, signature + _PUB_N_BYTE, _SALT_BYTE);
    sha2_chain_msg(correct, _PUB_M_BYTE, digest_salt, _HASH_LEN + _SALT_BYTE);

    uint8_t cc = 0;
    for (unsigned i = 0; i < _PUB_M_BYTE; i++) {
        cc |= (digest_ck[i] ^ correct[i]);
    }
    return (0 == cc) ? 0 : -1;
}
