#ifndef ABCRB_H_INCLUDED
#define ABCRB_H_INCLUDED

#include "rainbow-deps-api.h"  // for LEN_PRNG_CTX

// TODO FIXME All the object-like macros below SHALL expand to an integer
// constant expression, preferably without any external dependencies.
//
// TODO FIXME Remove unnecessary macros below.

#define RAINBOW_LEN_DGST 32  // number of bytes in a message digest
#define RAINBOW_LEN_SIG 64  // number of bytes in a digital signature
#define RAINBOW_LEN_SEED 32  // number of bytes in an account secret seed FIXME(js): change this length to variable 16..64

///  number of bytes in an account private key
///  sizeof( abcrb_key.h:abcrb_secretkey_t );
#define RAINBOW_LEN_PRV (RAINBOW_LEN_SEED + 4 * 3)

#define RAINBOW_LEN_PUB 152097  // number of bytes in an account public key
#define RAINBOW_LEN_PUBCHUNK 4096  // number of bytes in a public key chunk
#define RAINBOW_NUM_PUBCHUNKS 38  // number of chunks in a public key

// the working buffer byte lengths of for corresponding subroutines
#define RAINBOW_LEN_CTX_SEED2PRV (4608 + LEN_PRNG_CTX)  // 96*48

// LEN_PRNG_CTX + 200*_PUB_N_BYTE + sizeof(ckey_l2_vv)
// 384(344 actually)
#define RAINBOW_LEN_CTX_PRV2PUBCHUNK (384 + 200 * 48 + 34320)

/// buffer for singing.
//  sizeof(rainbow_16.h:rainbow_key) + ...........
//#define RAINBOW_LEN_CTX_SIGN (100208 + LEN_PRNG_CTX + 1024 + 544)
// LEN_PRNG_CTX*2 + s map + mat_l1 + res_l1_vv + 32*17(buffer for gaussian) + mat_l2 + prng_for_l2 + key_l2_vv
#define RAINBOW_LEN_CTX_SIGN (384 * 2 + 32 * 64 + 32 + 32 * 16 + 32 * 17 + 32 * 16 + 384 + 34320)

// need ~ 10 KB working memory
typedef struct {
    uint8_t linear_terms[96][32];  // [_PUB_N][_PUB_M_BYTE];
    uint8_t quad_terms[96][32];  // [_PUB_N][_PUB_M_BYTE];
    uint8_t const_terms[32];  // [_PUB_M_BYTE];
    uint8_t output[RAINBOW_LEN_CTX_PRV2PUBCHUNK];
} abcrb_getpubkey_ctx_t;

// TODO(js): rename prefix from "abcrb" to "drb" and write long name in README

// secret_seed => compressed_private_key (external dependencies: PRNG)
// TODO(js): Add one argument for variable length of seed `unsigned seedlen`
int abcrb_seed2prv(uint8_t *prv, const uint8_t *seed);

// compressed_private_key, message_digest => signature (external dependencies: PRNG, SHA256)
int abcrb_sign(uint8_t *sig, const uint8_t *prv, const uint8_t *dgst);

// compressed_private_key, chunk_id => non_standard_public_key_chunk
int abcrb_prv2pubchunk(uint8_t *pubchunk, const uint8_t *prv, unsigned chunkid);

// compressed_private_key, chunk_id => standard_public_key_chunk
// TODO(js): We don't need abcrb_getpubkey_clear() because the caller can do it itself
int abcrb_getpubkey_clear(abcrb_getpubkey_ctx_t *ctx);
int abcrb_getpubkey_prepare(abcrb_getpubkey_ctx_t *ctx, const uint8_t *prv);
int abcrb_getpubkey_gen(abcrb_getpubkey_ctx_t *ctx, uint8_t *pubchunk, const uint8_t *prv, int chunkid);

// all_non_standard_public_key_chunks => standard_public_key
int rainbow_pub_reorder(uint8_t *pub, const uint8_t *pubchunks);

// TODO(js): add prototype for signature verification subroutine

#endif
