#include "mpkc.h"
#include "blas.h"

#include <assert.h>  // FIXME(js): don't use assert() and don't deal with NDEBUG
#include <stdint.h>
#include <string.h>

void gf16mpkc_mq_eval_n_m(uint8_t *z, const uint8_t *pk_mat, const uint8_t *w, unsigned n, unsigned m) {
    assert(n <= 256);
    assert(m <= 256);
    uint8_t tmp[128];
    unsigned m_byte = (m + 1) >> 1;

    uint8_t _x[256];
    for (unsigned i = 0; i < n; i++) _x[i] = gf16v_get_ele(w, i);

    uint8_t *r = z;
    gf256v_set_zero(r, m_byte);
    for (unsigned i = 0; i < n; i++) {
        gf16v_madd(r, pk_mat, _x[i], m_byte);
        pk_mat += m_byte;
    }
    for (unsigned i = 0; i < n; i++) {
        gf256v_set_zero(tmp, m_byte);
        for (unsigned j = 0; j <= i; j++) {
            gf16v_madd(tmp, pk_mat, _x[j], m_byte);
            pk_mat += m_byte;
        }
        gf16v_madd(r, tmp, _x[i], m_byte);
    }
    gf256v_add(r, pk_mat, m_byte);
}
