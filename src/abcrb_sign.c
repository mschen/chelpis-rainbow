#include "abcrb.h"
#include "abcrb_key.h"
#include "blas.h"
#include "hash_utils.h"
#include "mpkc.h"
#include "rainbow-deps-api.h"

#include <string.h>

#define MAX_ATTEMPT_SIGN 128

/////////////////////////////

static void gen_l1_mat(uint8_t *mat, const ckey_l1 *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O1; i++) {
        gf16mat_prod(mat + i * _O1_BYTE, k->l1_vo[i], _O1_BYTE, _V1, v);
    }
    gf256v_add(mat, k->l1_o, _O1_BYTE * _O1);
}

static void gen_l2_mat(uint8_t *mat, const ckey_l2_o_vo *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O2; i++) {
        gf16mat_prod(mat + i * _O2_BYTE, k->l2_vo[i], _O2_BYTE, _V2, v);
    }
    gf256v_add(mat, k->l2_o, _O2_BYTE * _O1);
}

static unsigned linear_solver_32x32(uint8_t *buffer_for_gaussian, uint8_t *r, const uint8_t *mat_32x32, const uint8_t *cc) {
    uint8_t *mat = buffer_for_gaussian;
    for (unsigned i = 0; i < _O1; i++) {
        memcpy(mat + i * (_O1_BYTE + 1), mat_32x32 + i * (_O1_BYTE), _O1_BYTE);
        mat[i * (_O1_BYTE + 1) + _O1_BYTE] = gf16v_get_ele(cc, i);
    }
    unsigned r8 = gf16mat_gauss_elim(mat, _O1, _O1 + 2);
    for (unsigned i = 0; i < _O1; i++) {
        gf16v_set_ele(r, i, mat[i * (_O1_BYTE + 1) + _O1_BYTE]);
    }
    return r8;
}

/// algorithm 7
/// modified from : int rainbow_sign( uint8_t * signature , const uint8_t * _sk , const uint8_t * _digest )
static int _abcrb_sign(void *ctx, uint8_t *signature, const uint8_t *_sk, const uint8_t *_digest) {
    /// initialize expanded secret key

    uint8_t *prng_key = ctx;
    uint8_t *prng_ctx = prng_key + LEN_PRNG_CTX;
    uint8_t *mat_t = prng_ctx + LEN_PRNG_CTX;
    uint8_t *vec_t = mat_t + _PUB_N_BYTE * _PUB_N;

    const abcrb_secretkey_t *prv = (const abcrb_secretkey_t *) _sk;

    prng_setup(prng_key, prv->seed, RAINBOW_LEN_SEED);
    unsigned prng_idx = 0;

    /// initialize prng
    uint8_t prng_seed[64];
    memcpy(prng_seed, _digest, 32);
    sha256(prng_seed + 32, _sk, RAINBOW_LEN_PRV);

    prng_setup(prng_ctx, prng_seed, 64);

    uint8_t *mat_s = prng_ctx + LEN_PRNG_CTX;
    uint8_t *vec_s = mat_s + _PUB_M_BYTE * _PUB_M;
    uint8_t *mat_l1 = vec_s + _PUB_M_BYTE;
    uint8_t *res_l1_vv = mat_l1 + (_O1) * (_O1_BYTE);
    uint8_t *buffer_for_gaussian = res_l1_vv + _O1_BYTE;
    uint8_t *key_l1 = buffer_for_gaussian + _O1 * (_O1_BYTE + 1);
    ckey_l1 *k_l1 = (ckey_l1 *) key_l1;

    /// generate S map
    prng_idx = extract_s_map(prng_key, prng_idx, mat_s, vec_s, prv);
    /// dispose T map
    prng_idx = extract_t_map(prng_key, prng_idx, key_l1, buffer_for_gaussian, prv);
    /// generate cmap_l1
    prng_gen(prng_key, key_l1, sizeof(ckey_l1));  // XXX

    uint8_t vinegar[_V1_BYTE];  // XXX align
    unsigned l1_succ = 0;
    unsigned time = 0;
    while (!l1_succ) {
        if (MAX_ATTEMPT_SIGN == time)
            break;
        prng_gen(prng_ctx, vinegar, _V1_BYTE);
        gen_l1_mat(mat_l1, k_l1, vinegar);

        // make sure full-ranked mat_l1
        memcpy(buffer_for_gaussian, mat_l1, (_O1) * (_O1_BYTE));
        l1_succ = gf16mat_gauss_elim(buffer_for_gaussian, _O1, _O1);
        time++;
    }
    gf16mpkc_mq_eval_n_m(res_l1_vv, k_l1->l1_vv, vinegar, _V1, _O1);

    uint8_t *mat_l2 = buffer_for_gaussian + _O1 * (_O1_BYTE + 1);
    uint8_t *prng_for_l2 = mat_l2 + _O2 * (_O2_BYTE);
    memcpy(prng_for_l2, prng_key, LEN_PRNG_CTX);
    uint8_t *key_l2 = prng_for_l2 + LEN_PRNG_CTX;

    uint8_t temp_o1[_O1_BYTE] = {0};
    uint8_t temp_o2[_O2_BYTE];
    //// line 7 - 14
    uint8_t _z[_PUB_M_BYTE];
    uint8_t y[_PUB_M_BYTE];
    uint8_t x[_PUB_N_BYTE];
    uint8_t w[_PUB_N_BYTE];
    uint8_t digest_salt[_HASH_LEN + _SALT_BYTE] = {0};
    uint8_t *salt = digest_salt + _HASH_LEN;  // XXX align
    memcpy(digest_salt, _digest, _HASH_LEN);

    memcpy(x, vinegar, _V1_BYTE);
    unsigned succ = 0;
    while (!succ) {
        if (MAX_ATTEMPT_SIGN == time)
            break;
        prng_gen(prng_ctx, salt, _SALT_BYTE);
        sha2_chain_msg(_z, _PUB_M_BYTE, digest_salt, _HASH_LEN + _SALT_BYTE);  /// line 9

        gf256v_add(_z, vec_s, _PUB_M_BYTE);
        gf16mat_prod(y, mat_s, _PUB_M_BYTE, _PUB_M, _z);  /// line 10

        memcpy(temp_o1, res_l1_vv, _O1_BYTE);
        gf256v_add(temp_o1, y, _O1_BYTE);
        linear_solver_32x32(buffer_for_gaussian, x + _V1_BYTE, mat_l1, temp_o1);

        memcpy(prng_key, prng_for_l2, LEN_PRNG_CTX);  /// rewide prng
        prng_gen(prng_key, key_l2, sizeof(ckey_l2_o_vo));  // XXX
        ckey_l2_o_vo *k_l2_o_vo = (ckey_l2_o_vo *) key_l2;

        gen_l2_mat(mat_l2, k_l2_o_vo, x);

        prng_gen(prng_key, key_l2, sizeof(ckey_l2_vv));  // XXX
        ckey_l2_vv *k_l2_vv = (ckey_l2_vv *) key_l2;

        gf16mpkc_mq_eval_n_m(temp_o2, k_l2_vv->l2_vv, x, _V2, _O2);
        gf256v_add(temp_o2, y + _O1_BYTE, _O2_BYTE);
        succ = linear_solver_32x32(buffer_for_gaussian, x + _V2_BYTE, mat_l2, temp_o2);  /// line 13

        time++;
    };

    prng_setup(prng_key, prv->seed, RAINBOW_LEN_SEED);
    prng_idx = 0;
    prng_idx = extract_s_map(prng_key, prng_idx, mat_t, vec_t, prv);
    prng_idx = extract_t_map(prng_key, prng_idx, mat_t, vec_t, prv);

    gf256v_add(x, vec_t, _PUB_N_BYTE);
    gf16mat_prod(w, mat_t, _PUB_N_BYTE, _PUB_N, x);

    memset(signature, 0, _SIGNATURE_BYTE);

    gf256v_add(signature, w, _PUB_N_BYTE);
    gf256v_add(signature + _PUB_N_BYTE, salt, _SALT_BYTE);

    if (MAX_ATTEMPT_SIGN <= time)
        return -1;
    return 0;
}

int abcrb_sign(uint8_t *signature, const uint8_t *_sk, const uint8_t *_digest) {
    uint8_t ctx[RAINBOW_LEN_CTX_SIGN] = {0};
    return _abcrb_sign(ctx, signature, _sk, _digest);
}
