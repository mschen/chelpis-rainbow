#include "abcrb.h"
#include "abcrb_key.h"
#include "blas.h"
#include "mpkc.h"
#include "rainbow-deps-api.h"

#include <string.h>

#define MAX_ATTEMPT_SECRETKEY 128

#define IDX_QTERMS_REVLEX(xi, xj) ((xj) * ((xj) + 1) / 2 + (xi))

unsigned extract_s_map(void *prng_ctx, unsigned prng_idx, uint8_t *mat_s, uint8_t *vec_s, const abcrb_secretkey_t *prv) {
    while (prng_idx <= prv->prng_idx_s) {
        prng_gen(prng_ctx, mat_s, _PUB_M * _PUB_M_BYTE);
        prng_idx += _PUB_M * _PUB_M_BYTE;
    }
    prng_gen(prng_ctx, vec_s, _PUB_M_BYTE);
    prng_idx += _PUB_M_BYTE;
    return prng_idx;
}

unsigned extract_t_map(void *prng_ctx, unsigned prng_idx, uint8_t *mat_t, uint8_t *vec_t, const abcrb_secretkey_t *prv) {
    while (prng_idx <= prv->prng_idx_t) {
        prng_gen(prng_ctx, mat_t, _PUB_N * _PUB_N_BYTE);
        prng_idx += _PUB_M * _PUB_M_BYTE;
    }
    prng_gen(prng_ctx, vec_t, _PUB_N_BYTE);
    prng_idx += _PUB_N_BYTE;
    return prng_idx;
}

void _abcrb_expand_secretkey(void *prng_ctx, void *exp_sk, const abcrb_secretkey_t *prv) {
    prng_setup(prng_ctx, prv->seed, RAINBOW_LEN_SEED);
    unsigned prng_idx = 0;

    ns_rainbow_key *sk = (ns_rainbow_key *) exp_sk;
    // generate S map
    prng_idx = extract_s_map(prng_ctx, prng_idx, sk->mat_s, sk->vec_s, prv);
    // generate S map
    prng_idx = extract_t_map(prng_ctx, prng_idx, sk->mat_t, sk->vec_t, prv);

    // generate C map
    //prng_gen(prng_ctx, (uint8_t *) &sk->ckey, sizeof(ns_rainbow_ckey));
    uint8_t *cptr = (uint8_t *) &sk->ckey;
    prng_gen(prng_ctx, cptr, sizeof(ckey_l1));  // l1  // XXX
    cptr += sizeof(ckey_l1);

    prng_gen(prng_ctx, cptr, sizeof(ckey_l2_o_vo));  // l2-o+vo  // XXX
    cptr += sizeof(ckey_l2_o_vo);

    prng_gen(prng_ctx, cptr, sizeof(ckey_l2_vv));  // l2-vv  // XXX
    cptr += sizeof(ckey_l2_vv);
}

static int _abcrb_seed2prv(void *ctx, uint8_t *prv, const uint8_t *seed) {
    // initailize abcrb_secretkey_t
    abcrb_secretkey_t *abcrb_prv = (abcrb_secretkey_t *) prv;
    memcpy(abcrb_prv->seed, seed, RAINBOW_LEN_SEED);
    abcrb_prv->prng_idx_s = 0;
    abcrb_prv->prng_idx_t = 0;
    abcrb_prv->prng_idx_c = 0;

    uint8_t *prng_ctx = (uint8_t *) ctx;
    prng_setup(prng_ctx, seed, RAINBOW_LEN_SEED);
    unsigned prng_idx = 0;

    uint8_t *mat_96x96 = prng_ctx + LEN_PRNG_CTX;  // XXX align?

    int i;

    // check S full-ranked
    for (i = 0; i < MAX_ATTEMPT_SECRETKEY; i++) {
        abcrb_prv->prng_idx_s = prng_idx;
        prng_gen(prng_ctx, mat_96x96, _PUB_M * _PUB_M_BYTE);
        prng_idx += _PUB_M * _PUB_M_BYTE;

        uint8_t r = gf16mat_gauss_elim(mat_96x96, _PUB_M, _PUB_M);
        if (r) {
            break;
        }
    }
    if (i >= MAX_ATTEMPT_SECRETKEY) {
        return -1;
    }

    // prng_gen( prng_ctx, sk->vec_s, _PUB_M_BYTE );
    prng_gen(prng_ctx, mat_96x96, _PUB_M_BYTE);
    prng_idx += _PUB_M_BYTE;

    // check S full-ranked
    for (i = 0; i < MAX_ATTEMPT_SECRETKEY; i++) {
        abcrb_prv->prng_idx_t = prng_idx;
        prng_gen(prng_ctx, mat_96x96, _PUB_N * _PUB_N_BYTE);
        prng_idx += _PUB_N * _PUB_N_BYTE;

        uint8_t r = gf16mat_gauss_elim(mat_96x96, _PUB_N, _PUB_N);
        if (r) {
            break;
        }
    }
    if (i >= MAX_ATTEMPT_SECRETKEY) {
        return -1;
    }

    // prng_gen( prng_ctx, mat_96x96, _PUB_N_BYTE );  // no need to generate
    prng_idx += _PUB_N_BYTE;

    abcrb_prv->prng_idx_c = prng_idx;

    // TODO

    return 0;
}

int abcrb_seed2prv(uint8_t *prv, const uint8_t *seed) {
    uint8_t ctx[RAINBOW_LEN_CTX_SEED2PRV] = {0};
    return _abcrb_seed2prv(ctx, prv, seed);
}

// description for the source of the interpolation.
typedef struct {
    uint8_t n_ele;
    uint8_t coef;
    uint8_t idx[2];
} src_interp;

static unsigned fill_out_prepare_srcs(src_interp *srcs) {
    unsigned w_idx = 0;
    // linear terms
    for (unsigned i = 0; i < _PUB_N; i++) {
        srcs[w_idx].n_ele = 1;
        srcs[w_idx].coef = 1;
        srcs[w_idx].idx[0] = i;
        w_idx++;
    }
    // quadratic terms
    for (unsigned i = 0; i < _PUB_N; i++) {
        srcs[w_idx].n_ele = 1;
        srcs[w_idx].coef = 2;
        srcs[w_idx].idx[0] = i;
        w_idx++;
    }
    // constant terms
    srcs[w_idx].n_ele = 0;
    srcs[w_idx].coef = 0;
    srcs[w_idx].idx[0] = 0;
    w_idx++;
    return w_idx;
}

static unsigned fill_out_srcs(src_interp *srcs, unsigned chunkid) {
    if ((unsigned) -1 == chunkid) {
        return fill_out_prepare_srcs(srcs);
    }

    const unsigned lines_per_chunk = RAINBOW_LEN_PUBCHUNK / _PUB_M_BYTE;
    unsigned first_line = chunkid * lines_per_chunk;
    unsigned w_idx = 0;
    if (TERMS_QUAD_POLY(_PUB_N) <= first_line) {
        return w_idx;
    }

    unsigned idx = 0;
    if (idx + _PUB_N <= first_line) {
        idx += _PUB_N;
    } else {
        for (unsigned i = 0; i < _PUB_N; i++) {
            if (idx < first_line) {
                idx++;
                continue;
            }
            srcs[w_idx].n_ele = 1;
            srcs[w_idx].coef = 1;
            srcs[w_idx].idx[0] = i;
            w_idx++;
            if (lines_per_chunk == w_idx) {
                return w_idx;
            }
        }
    }
    for (unsigned i = 0; i < _PUB_N; i++) {
        if (idx + (i + 1) <= first_line) {
            idx += (i + 1);
            continue;
        }
        for (unsigned j = 0; j < i; j++) {
            if (idx < first_line) {
                idx++;
                continue;
            }
            srcs[w_idx].n_ele = 2;
            srcs[w_idx].coef = 1;
            srcs[w_idx].idx[0] = i;
            srcs[w_idx].idx[1] = j;
            w_idx++;
            if (lines_per_chunk == w_idx) {
                return w_idx;
            }
        }
        if (idx < first_line) {
            idx++;
            continue;
        }
        srcs[w_idx].n_ele = 1;
        srcs[w_idx].coef = 2;
        srcs[w_idx].idx[0] = i;
        w_idx++;
        if (lines_per_chunk == w_idx) {
            return w_idx;
        }
    }
    srcs[w_idx].n_ele = 0;
    srcs[w_idx].coef = 0;
    srcs[w_idx].idx[0] = 0;
    w_idx++;

    return w_idx;
}

////////////////////////////////////////////////////////////////////////////////

static void perform_t_map(uint8_t *results_t, const uint8_t *mat_t, const uint8_t *vec_t, const src_interp *srcs, unsigned n_rows) {
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *x = (uint8_t *) (results_t + i * _PUB_N_BYTE);
        //gf256v_add(x, vec_t, _PUB_N_BYTE);
        memcpy(x, vec_t, _PUB_N_BYTE);
        // perform the matrix-vector multiplication
        for (unsigned j = 0; j < srcs[i].n_ele; j++) {
            if (1 == srcs[i].coef) {
                unsigned idx = srcs[i].idx[j];
                gf256v_add(x, mat_t + idx * _PUB_N_BYTE, _PUB_N_BYTE);
            } else {
                unsigned idx = srcs[i].idx[j];
                gf16v_madd(x, mat_t + idx * _PUB_N_BYTE, srcs[i].coef, _PUB_N_BYTE);
            }
        }
    }
}

static void my_gen_l1_mat(uint8_t *mat, const ckey_l1 *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O1; i++) {
        gf16mat_prod(mat + i * _O1_BYTE, k->l1_vo[i], _O1_BYTE, _V1, v);
        gf256v_add(mat + i * _O1_BYTE, k->l1_o + i * _O1_BYTE, _O1_BYTE);
    }
}

static void my_gen_l2_mat(uint8_t *mat, const ckey_l2_o_vo *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O2; i++) {
        gf16mat_prod(mat + i * _O2_BYTE, k->l2_vo[i], _O2_BYTE, _V2, v);
        gf256v_add(mat + i * _O2_BYTE, k->l2_o + i * _O2_BYTE, _O2_BYTE);
    }
}

// private key, chunk id => a chunk of public key (only 4096 bytes)
// no external dependencies
// takes at most 10 MCU seconds (about 7 MCU minutes for full public key)
static int _abcrb_prv2pubchunk(void *ctx, uint8_t *pubchunk, const uint8_t *_prv, unsigned chunkid) {
    const abcrb_secretkey_t *prv = (const abcrb_secretkey_t *) _prv;
    uint8_t *byte_buffer = (uint8_t *) ctx;
    uint8_t *prng_ctx = byte_buffer;
    prng_setup(prng_ctx, prv->seed, RAINBOW_LEN_SEED);
    unsigned prng_idx = 0;

    //const unsigned max_eles = (RAINBOW_LEN_PUBCHUNK) / (_PUB_M_BYTE);
    const unsigned max_eles = 200;  // for chunk -1
    // results of the T map
    uint8_t *res_t = (byte_buffer + LEN_PRNG_CTX);
    // generating the T map
    uint8_t *vec_t = res_t + max_eles * _PUB_N_BYTE;
    uint8_t *mat_t = vec_t + _PUB_N_BYTE;
    uint8_t *mat_ivs_t = mat_t + _PUB_N_BYTE * _PUB_N;
    uint8_t *temp_mat = mat_ivs_t + _PUB_N_BYTE * _PUB_N;
    prng_idx = extract_s_map(prng_ctx, prng_idx, mat_t, vec_t, prv);  // can not temporarily store the S map.
    prng_idx = extract_t_map(prng_ctx, prng_idx, mat_ivs_t, vec_t, prv);
    gf16mat_inv(mat_t, mat_ivs_t, _PUB_N, temp_mat);
    // perform the T map
    src_interp *srcs = (src_interp *) mat_ivs_t;
    unsigned n_rows = fill_out_srcs(srcs, chunkid);
    if (0 == n_rows) return -1;  // out of correct chunkids ??
    perform_t_map(res_t, mat_t, vec_t, srcs, n_rows);

    // central map
    // fill out the ckey
    uint8_t *cptr = vec_t;
    // perform the c-map:  L1
    prng_gen(prng_ctx, cptr, sizeof(ckey_l1));  // l1  // XXX
    ckey_l1 *k_l1 = (ckey_l1 *) cptr;
    uint8_t *mat1 = cptr + sizeof(ckey_l1);
    uint8_t *temp = mat1 + _O1 * _O1_BYTE;
    // results of the central map are temporarily stored in the output ``pubchunk''
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *a = res_t + i * _PUB_N_BYTE;
        uint8_t *r = pubchunk + i * _PUB_M_BYTE;
        my_gen_l1_mat(mat1, k_l1, a);
        gf16rowmat_prod(r, mat1, _O1, _O1_BYTE, a + _V1_BYTE);
        gf16mpkc_mq_eval_n_m(temp, k_l1->l1_vv, a, _V1, _O1);
        gf256v_add(r, temp, _O1_BYTE);
    }
    // perfrom the c-map: L2-o+vo
    prng_gen(prng_ctx, cptr, sizeof(ckey_l2_o_vo));  // l2-o+vo  // XXX
    ckey_l2_o_vo *k_l2_o_vo = (ckey_l2_o_vo *) cptr;
    uint8_t *mat2 = cptr + sizeof(ckey_l2_o_vo);
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *a = res_t + i * _PUB_N_BYTE;
        uint8_t *r = pubchunk + i * _PUB_M_BYTE + _O1_BYTE;
        my_gen_l2_mat(mat2, k_l2_o_vo, a);
        gf16rowmat_prod(r, mat2, _O2, _O2_BYTE, a + _V2_BYTE);
    }
    // perfrom the c-map: L2-vv
    prng_gen(prng_ctx, cptr, sizeof(ckey_l2_vv));  // l2-vv  // XXX
    ckey_l2_vv *k_l2_vv = (ckey_l2_vv *) cptr;
    temp = cptr + sizeof(ckey_l2_vv);
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *a = res_t + i * _PUB_N_BYTE;
        uint8_t *r = pubchunk + i * _PUB_M_BYTE + _O1_BYTE;
        gf16mpkc_mq_eval_n_m(temp, k_l2_vv->l2_vv, a, _V2, _O2);
        gf256v_add(r, temp, _O2_BYTE);
    }

    // rewind prng
    prng_setup(prng_ctx, prv->seed, RAINBOW_LEN_SEED);
    prng_idx = 0;
    // generating the S map
    uint8_t *vec_s = byte_buffer + LEN_PRNG_CTX;
    uint8_t *mat_s = vec_s + _PUB_M_BYTE;
    uint8_t *mat_ivs_s = mat_s + _PUB_M_BYTE * _PUB_M;
    temp_mat = mat_ivs_s + _PUB_M_BYTE * _PUB_M;
    prng_idx = extract_s_map(prng_ctx, prng_idx, mat_ivs_s, vec_s, prv);
    gf16mat_inv(mat_s, mat_ivs_s, _PUB_M, temp_mat);
    // perform the S map
    uint8_t temp_y[_PUB_M_BYTE];
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *z = pubchunk + i * _PUB_M_BYTE;
        memcpy(temp_y, z, _PUB_M_BYTE);
        gf16mat_prod(z, mat_s, _PUB_M_BYTE, _PUB_M, temp_y);
        gf256v_add(z, vec_s, _PUB_M_BYTE);
    }
    return 0;
}

int abcrb_prv2pubchunk(uint8_t *pubchunk, const uint8_t *_prv, unsigned chunkid) {
    uint8_t ctx[RAINBOW_LEN_CTX_PRV2PUBCHUNK] = {0};
    return _abcrb_prv2pubchunk(ctx, pubchunk, _prv, chunkid);
}

///////////////////////////////////////////////////////////////////////////////

//
// presuming: run in standard PC
//
int rainbow_pub_reorder(uint8_t *pub, const uint8_t *pubchunks) {
    uint8_t *poly = pub;
    const uint8_t *linear_terms = pubchunks;
    const uint8_t *quad_terms = pubchunks + _PUB_M_BYTE * _PUB_N;
    const uint8_t *const_terms = pubchunks + _PUB_M_BYTE * (TERMS_QUAD_POLY(_PUB_N) - 1);

    uint8_t tmp_r0[_PUB_M_BYTE] = {0};
    uint8_t tmp_r1[_PUB_M_BYTE] = {0};
    uint8_t tmp_r2[_PUB_M_BYTE] = {0};

    const unsigned n_var = _PUB_N;
    for (unsigned i = 0; i < n_var; i++) {
        // quad_poly(tmp_r0, key, tmp);  // v + v^2
        memcpy(tmp_r0, linear_terms + i * _PUB_M_BYTE, _PUB_M_BYTE);
        gf256v_add(tmp_r0, const_terms, _PUB_M_BYTE);

        memcpy(tmp_r2, tmp_r0, _PUB_M_BYTE);
        gf16v_mul_scalar(tmp_r0, gf16_mul(2, 2), _PUB_M_BYTE);  // 3v + 3v^2

        // quad_poly( tmp_r1, key, tmp ); // 2v + 3v^2
        memcpy(tmp_r1, quad_terms + _PUB_M_BYTE * IDX_QTERMS_REVLEX(i, i), _PUB_M_BYTE);
        gf256v_add(tmp_r1, const_terms, _PUB_M_BYTE);

        gf256v_add(tmp_r0, tmp_r1, _PUB_M_BYTE);  // v
        gf256v_add(tmp_r2, tmp_r0, _PUB_M_BYTE);  // v^2
        memcpy(poly + _PUB_M_BYTE * i, tmp_r0, _PUB_M_BYTE);
        memcpy(poly + _PUB_M_BYTE * (n_var + IDX_QTERMS_REVLEX(i, i)), tmp_r2, _PUB_M_BYTE);
    }

    for (unsigned i = 1; i < n_var; i++) {
        unsigned base_idx = n_var + IDX_QTERMS_REVLEX(0, i);
        memcpy(tmp_r1, poly + _PUB_M_BYTE * i, _PUB_M_BYTE);
        memcpy(tmp_r2, poly + _PUB_M_BYTE * (n_var + IDX_QTERMS_REVLEX(i, i)), _PUB_M_BYTE);

        for (unsigned j = 0; j < i; j++) {
            // quad_poly(tmp_r0, key, tmp);  // v1 + v1^2 + v2 + v2^2 + v1v2
            memcpy(tmp_r0, linear_terms + (base_idx + j) * _PUB_M_BYTE, _PUB_M_BYTE);
            gf256v_add(tmp_r0, const_terms, _PUB_M_BYTE);

            gf256v_add(tmp_r0, tmp_r1, _PUB_M_BYTE);
            gf256v_add(tmp_r0, tmp_r2, _PUB_M_BYTE);
            gf256v_add(tmp_r0, poly + _PUB_M_BYTE * j, _PUB_M_BYTE);
            gf256v_add(tmp_r0, poly + _PUB_M_BYTE * (n_var + IDX_QTERMS_REVLEX(j, j)), _PUB_M_BYTE);

            memcpy(poly + _PUB_M_BYTE * (base_idx + j), tmp_r0, _PUB_M_BYTE);
        }
    }
    memcpy(poly + _PUB_M_BYTE * (TERMS_QUAD_POLY(_PUB_N) - 1), const_terms, _PUB_M_BYTE);
    poly[_PUB_M_BYTE * TERMS_QUAD_POLY(_PUB_N)] = 16;

    return 0;
}

int abcrb_getpubkey_clear(abcrb_getpubkey_ctx_t *ctx) {
    memset(ctx, 0, sizeof(abcrb_getpubkey_ctx_t));
    return 0;
}

int abcrb_getpubkey_prepare(abcrb_getpubkey_ctx_t *_ctx, const uint8_t *_prv) {
    _abcrb_prv2pubchunk(_ctx->output, _ctx->linear_terms[0], _prv, -1);

    abcrb_getpubkey_ctx_t *pubchunk_ctx = _ctx;

    uint8_t tmp_r0[_PUB_M_BYTE] = {0};
    uint8_t tmp_r1[_PUB_M_BYTE] = {0};
    uint8_t tmp_r2[_PUB_M_BYTE] = {0};

    const unsigned n_var = _PUB_N;
    for (unsigned i = 0; i < n_var; i++) {
        // quad_poly(tmp_r0, key, tmp);  // v + v^2
        memcpy(tmp_r0, pubchunk_ctx->linear_terms[i], _PUB_M_BYTE);
        gf256v_add(tmp_r0, pubchunk_ctx->const_terms, _PUB_M_BYTE);

        memcpy(tmp_r2, tmp_r0, _PUB_M_BYTE);
        gf16v_mul_scalar(tmp_r0, gf16_mul(2, 2), _PUB_M_BYTE);  // 3v + 3v^2

        // quad_poly(tmp_r1, key, tmp);  // 2v + 3v^2
        memcpy(tmp_r1, pubchunk_ctx->quad_terms[i], _PUB_M_BYTE);
        gf256v_add(tmp_r1, pubchunk_ctx->const_terms, _PUB_M_BYTE);

        gf256v_add(tmp_r0, tmp_r1, _PUB_M_BYTE);  // v
        gf256v_add(tmp_r2, tmp_r0, _PUB_M_BYTE);  // v^2
        memcpy(pubchunk_ctx->linear_terms[i], tmp_r0, _PUB_M_BYTE);
        memcpy(pubchunk_ctx->quad_terms[i], tmp_r2, _PUB_M_BYTE);
    }

    return 0;
}

int abcrb_getpubkey_gen(abcrb_getpubkey_ctx_t *ctx, uint8_t *pubchunk, const uint8_t *prv, int chunkid) {
    abcrb_getpubkey_ctx_t *prepared_ctx = ctx;

    ctx = (abcrb_getpubkey_ctx_t *) ctx->output;

    _abcrb_prv2pubchunk(ctx, pubchunk, prv, chunkid);

    uint8_t tmp_r0[_PUB_M_BYTE] = {0};

    src_interp srcs[128];
    unsigned n_rows = fill_out_srcs(srcs, chunkid);

    for (unsigned i = 0; i < n_rows; i++) {
        if (srcs[i].n_ele == 1 && srcs[i].coef == 1) {
            memcpy(pubchunk + _PUB_M_BYTE * i, prepared_ctx->linear_terms[srcs[i].idx[0]], _PUB_M_BYTE);
        } else if (srcs[i].n_ele == 1 && srcs[i].coef == 2) {
            memcpy(pubchunk + _PUB_M_BYTE * i, prepared_ctx->quad_terms[srcs[i].idx[0]], _PUB_M_BYTE);
        } else if (srcs[i].n_ele == 0) {
            memcpy(pubchunk + _PUB_M_BYTE * i, prepared_ctx->const_terms, _PUB_M_BYTE);
        } else {
            unsigned ii = srcs[i].idx[0];
            unsigned jj = srcs[i].idx[1];
            // quad_poly(tmp_r0, key, tmp);  // v1 + v1^2 + v2 + v2^2 + v1v2
            memcpy(tmp_r0, pubchunk + _PUB_M_BYTE * i, _PUB_M_BYTE);
            gf256v_add(tmp_r0, prepared_ctx->const_terms, _PUB_M_BYTE);
            gf256v_add(tmp_r0, prepared_ctx->linear_terms[ii], _PUB_M_BYTE);
            gf256v_add(tmp_r0, prepared_ctx->linear_terms[jj], _PUB_M_BYTE);
            gf256v_add(tmp_r0, prepared_ctx->quad_terms[ii], _PUB_M_BYTE);
            gf256v_add(tmp_r0, prepared_ctx->quad_terms[jj], _PUB_M_BYTE);

            memcpy(pubchunk + _PUB_M_BYTE * i, tmp_r0, _PUB_M_BYTE);
        }
    }

    // last byte "0x10" in the publick key
    if (37 == chunkid) {
        pubchunk[_PUB_M_BYTE * n_rows] = 0x10;
    }

    return 0;
}
