#ifndef _SHA256_UTILS_H_
#define _SHA256_UTILS_H_

#include <stdint.h>

int sha2_chain_msg(uint8_t *digest, unsigned n_digest, const uint8_t *m, unsigned long long mlen);

#endif
