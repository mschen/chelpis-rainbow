#ifndef RAINBOW_DEPS_API_H_INCLUDED
#define RAINBOW_DEPS_API_H_INCLUDED

#include "mbedtls_ctr_drbg.h"

#include <stdint.h>


// NOTE: change buf size will change gen() behavior.
typedef struct {
    mbedtls_ctr_drbg_context ctr_drbg_ctx;
    uint8_t buf[1024]; // MUST <= MBEDTLS_CTR_DRBG_MAX_REQUEST
    unsigned used;
} prng_ctx_t;


#define LEN_PRNG_CTX sizeof(mbedtls_ctr_drbg_context)

void sha256(uint8_t *dgst, const uint8_t *msg, unsigned msglen);

void prng_setup(void *prng_ctx, const void *seed, unsigned seedlen);
void prng_gen(void *prng_ctx, void *out, unsigned numbytes);


void prng_setup2(prng_ctx_t *ctx, const uint8_t *seed, size_t seedlen);
void prng_gen2(prng_ctx_t *ctx, uint8_t *out, size_t numbytes);

#endif
