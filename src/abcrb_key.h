#ifndef _ABCRB_KEY_H_
#define _ABCRB_KEY_H_

#define ABCRB_CONFIG_MAX_ATTEMP_PRNG 100

#include "abcrb.h"  /// for macro: RAINBOW_LEN_SEED
#include "rainbow_config.h"

///
///  The secret key format for abcrb
///
typedef struct {
    uint8_t seed[RAINBOW_LEN_SEED];
    uint32_t prng_idx_c;  // XXX to be portable use uint32_t
    uint32_t prng_idx_s;
    uint32_t prng_idx_t;
} abcrb_secretkey_t;

///
/// The secret key of original nist-rainbow submission
///
typedef struct {
    uint8_t l1_o[_O1 * _O1_BYTE];
    uint8_t l1_vo[_O1][_V1 * _O1_BYTE];
    uint8_t l1_vv[TERMS_QUAD_POLY(_V1) * _O1_BYTE];

    uint8_t l2_o[_O2 * _O2_BYTE];
    uint8_t l2_vo[_O2][_V2 * _O2_BYTE];
    uint8_t l2_vv[TERMS_QUAD_POLY(_V2) * _O2_BYTE];
} ns_rainbow_ckey;

typedef struct {
    uint8_t mat_t[_PUB_N * _PUB_N_BYTE];
    uint8_t vec_t[_PUB_N_BYTE];
    uint8_t mat_s[_PUB_M * _PUB_M_BYTE];
    uint8_t vec_s[_PUB_M_BYTE];

    ns_rainbow_ckey ckey;
} ns_rainbow_key;

///
/// Division of the ckey
///
typedef struct {
    uint8_t l1_o[_O1 * _O1_BYTE];
    uint8_t l1_vo[_O1][_V1 * _O1_BYTE];
    uint8_t l1_vv[TERMS_QUAD_POLY(_V1) * _O1_BYTE];
} ckey_l1;

/// sizeof(ckey_l1)
#define SIZE_CKEY_L1 (25872)

typedef struct {
    uint8_t l2_o[_O2 * _O2_BYTE];
    uint8_t l2_vo[_O2][_V2 * _O2_BYTE];
} ckey_l2_o_vo;

/// sizeof(ckey_l2_o_vo)
#define SIZE_CKEY_L2_O_VO (33280)

typedef struct {
    uint8_t l2_vv[TERMS_QUAD_POLY(_V2) * _O2_BYTE];
} ckey_l2_vv;

/// sizeof(ckey_l2_vv)
#define SIZE_CKEY_L2_VV (34320)

//
// abcrb_secretkey => expanded rainbow secret key(rainbow_key)
// external dependencies: PRNG
// note: ctx <- buffer for prng
void _abcrb_expand_secretkey(void *prng_ctx, void *exp_sk, const abcrb_secretkey_t *prv);

unsigned extract_s_map(void *prng_ctx, unsigned prng_idx, uint8_t *mat_s, uint8_t *vec_s, const abcrb_secretkey_t *prv);

unsigned extract_t_map(void *prng_ctx, unsigned prng_idx, uint8_t *mat_t, uint8_t *vec_t, const abcrb_secretkey_t *prv);

#endif
