#include "hash_utils.h"
#include "rainbow-deps-api.h"
#include "rainbow_config.h"  // for _HASH_LEN=32

static inline void sha2_chain(uint8_t *d2, const uint8_t *d1) {
    sha256(d2, d1, _HASH_LEN);
}

static inline int expand_sha2(uint8_t *digest, unsigned n_digest, const uint8_t *hash) {
    if (_HASH_LEN >= n_digest) {
        for (unsigned i = 0; i < n_digest; i++) {
            digest[i] = hash[i];
        }
        return 0;
    } else {
        for (unsigned i = 0; i < _HASH_LEN; i++) {
            digest[i] = hash[i];
        }
        n_digest -= _HASH_LEN;
    }

    while (_HASH_LEN <= n_digest) {
        sha2_chain(digest + _HASH_LEN, digest);
        n_digest -= _HASH_LEN;
        digest += _HASH_LEN;
    }
    uint8_t temp[_HASH_LEN];
    if (n_digest) {
        sha2_chain(temp, digest);
        for (unsigned i = 0; i < n_digest; i++) {
            digest[_HASH_LEN + i] = temp[i];
        }
    }
    return 0;
}

int sha2_chain_msg(uint8_t *digest, unsigned n_digest, const uint8_t *m, unsigned long long mlen) {
    uint8_t hash[_HASH_LEN];
    sha256(hash, m, mlen);
    expand_sha2(digest, n_digest, hash);
    return 0;
}
