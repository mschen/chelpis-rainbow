#include "mbedtls_ctr_drbg.h"
#include "rainbow-deps-api.h"

#include "abcrb.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>

static int dummy_entropy(void *p_entropy, uint8_t *output, size_t len) {
    // no external entropy retrieval allowed
    if (len != 0) return -1;
    (void) p_entropy;
    (void) output;
    return 0;
}

#define HEXDUMP(ptr, len) hexdump(__FILE__, __LINE__, #ptr, (ptr), (len))
static void hexdump(const char *file, int line, const char *ptr_expr, const void *ptr, unsigned len) {
    printf("%s:%d:\t%s ==> [%u bytes] [", file, line, ptr_expr, len);
    char hexstr_buf[33];
    unsigned off, idx;
    unsigned char val, val_hi, val_lo;
    for (off = 0; off < len; off += 16) {
        for (idx = off; idx < len && idx < off + 16; ++idx) {
            val = ((const unsigned char *) ptr)[idx];
            val_hi = val >> 4;
            val_lo = val & 15;
            hexstr_buf[(idx - off) * 2 + 0] = "0123456789ABCDEF"[val_hi];
            hexstr_buf[(idx - off) * 2 + 1] = "0123456789ABCDEF"[val_lo];
            hexstr_buf[(idx - off) * 2 + 2] = 0;
        }
        printf("%s", hexstr_buf);
    }
    printf("]\r\n");
}

void prng_setup2(prng_ctx_t *ctx, const uint8_t *seed, size_t seedlen) {
    // FIXME: `ctx` will be interpreted as (mbedtls_ctr_drbg_context *) <= ALIGNMENT ISSUE!!!  (Done ???)
    // XXX: `seed` must be a pointer to a buffer of length `seedlen` bytes
    // TODO: `seedlen` must be in the range 16 .. 64 (WHY?)

    mbedtls_ctr_drbg_init(&ctx->ctr_drbg_ctx);
    mbedtls_ctr_drbg_seed(&ctx->ctr_drbg_ctx, dummy_entropy, 0, seed, seedlen);
    ctx->used = sizeof(ctx->buf);
}

void prng_gen2(prng_ctx_t *ctx, uint8_t *out, size_t numbytes) {
    while (numbytes) {
        size_t nb = (numbytes > sizeof(ctx->buf) - ctx->used) ? sizeof(ctx->buf) - ctx->used : numbytes;
        memcpy(out, &ctx->buf[ctx->used], nb);
        out += nb;
        numbytes -= nb;

        ctx->used += nb;

        // Fill up buffer if consumed
        if (numbytes != 0 /* only generate when neccesary */ && ctx->used == sizeof(ctx->buf))
        {
            ctx->used = 0;
            mbedtls_ctr_drbg_random(&ctx->ctr_drbg_ctx, ctx->buf, sizeof(ctx->buf));
            HEXDUMP(ctx->buf, sizeof(ctx->buf));
        }
    }
}


void prng_setup(void *prng_ctx, const void *seed, unsigned seedlen) {
    // FIXME: `prng_ctx` will be interpreted as (mbedtls_ctr_drbg_context *) <= ALIGNMENT ISSUE!!!  (Done ???)
    // XXX: `seed` must be a pointer to a buffer of length `seedlen` bytes
    // TODO: `seedlen` must be in the range 16 .. 64

    mbedtls_ctr_drbg_context local_ctx;
    mbedtls_ctr_drbg_init(&local_ctx);
    mbedtls_ctr_drbg_seed(&local_ctx, dummy_entropy, 0, seed, seedlen);
    memcpy(prng_ctx, &local_ctx, sizeof(mbedtls_ctr_drbg_context));
    memset(&local_ctx, 0, sizeof(mbedtls_ctr_drbg_context));
}

void prng_gen(void *prng_ctx, void *out, unsigned numbytes) {
    // TODO: `numbytes` must satisfy (numbytes % 16 == 0)

    mbedtls_ctr_drbg_context local_ctx;
    memcpy(&local_ctx, prng_ctx, sizeof (mbedtls_ctr_drbg_context));

    uint8_t *out8 = out;
    while (numbytes) {
        unsigned nb = (numbytes > MBEDTLS_CTR_DRBG_MAX_REQUEST) ? MBEDTLS_CTR_DRBG_MAX_REQUEST : numbytes;
        mbedtls_ctr_drbg_random(&local_ctx, out8, nb);
        out8 += nb;
        numbytes -= nb;
    }

    memcpy(prng_ctx, &local_ctx, sizeof (mbedtls_ctr_drbg_context));
    memset(&local_ctx, 0, sizeof (mbedtls_ctr_drbg_context));
}
