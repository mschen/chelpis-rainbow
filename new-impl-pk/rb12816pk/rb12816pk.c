#include "stdalign.h"
#include "rb12816pk.h"

// This header file provides the prototype for function sha256()
#include "../external/external.h"

#include "rainbow_config.h"
#include "mpkc.h"

#include <string.h>   /// for memcpy()


int rb12816pk_verify(const unsigned char *pubkey, const unsigned char *digest, const unsigned char *signature) {

    alignas(4) uint8_t digest_ck[_PUB_M_BYTE];
    //rainbow_pubmap(digest_ck, pk, signature);
    gf16mpkc_mq_eval_n_m(digest_ck, pubkey, signature, _PUB_N, _PUB_M);

    alignas(4) uint8_t correct[_PUB_M_BYTE];
    alignas(4) uint8_t digest_salt[_HASH_LEN + _SALT_BYTE];
    memcpy(digest_salt, digest, _HASH_LEN);
    memcpy(digest_salt + _HASH_LEN, signature + _PUB_N_BYTE, _SALT_BYTE);
    //sha2_chain_msg(correct, _PUB_M_BYTE, digest_salt, _HASH_LEN + _SALT_BYTE);
    sha256(correct, digest_salt, _HASH_LEN + _SALT_BYTE);

    uint8_t cc = 0;
    for (unsigned i = 0; i < _PUB_M_BYTE; i++) {
        cc |= (digest_ck[i] ^ correct[i]);
    }
    return (0 == cc) ? 0 : RB12816PK_ERROR_BAD_SIG;
}
