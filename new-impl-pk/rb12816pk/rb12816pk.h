#ifndef RB12816PK_H_INCLUDED
#define RB12816PK_H_INCLUDED

#define RB12816PK_BYTELEN_PUB 152097
#define RB12816PK_BYTELEN_DGST 32
#define RB12816PK_BYTELEN_SIG 64

#define RB12816PK_ERROR_NONE 0
#define RB12816PK_ERROR_BAD_SIG (-1)
#define RB12816PK_ERROR_OTHER_REASON (-2)

int rb12816pk_verify(const unsigned char *pubkey, const unsigned char *dgst, const unsigned char *sig);

#endif
