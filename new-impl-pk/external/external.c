#include "external.h"

#include "mbedtls/sha256.h"

int sha256(unsigned char *dgst, const unsigned char *msg, unsigned long msglen) {
    int err = mbedtls_sha256_ret(msg, msglen, dgst, 0);
    if (err) return -1;
    return 0;
}
