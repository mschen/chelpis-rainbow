#ifndef EXTERNAL_H_INCLUDED
#define EXTERNAL_H_INCLUDED

int sha256(unsigned char *dgst, const unsigned char *msg, unsigned long msglen);

#endif
