### Three use cases:

1. for bootloader
    * verify (firmware signature for secure boot)

2. for firmware application
    * deterministic key derivation
    * deterministic signing operation

3. for PC (Chelpis / consumer)
    * verify
    * key generation (bootstrapping Chelpis root of trust / hot wallet)
    * deterministic key derivation (release firmware update / hot wallet)
    * deterministic signing operation (release firmware update / hot wallet)

### Portable secure coding guidelines:

Instead of using `(void *)` or `(unsigned char *)` or similar types, we should
simply use pointer to `uintN_t` or pointer to struct types if that is more
appropriate.  See
<https://gustedt.wordpress.com/2014/04/02/dont-use-casts-i/> for more info.

Otherwise, we need to do more on: aliasing issues / alignment issues / check
on buffer size or boundary.

For example, the function `abcrb_prv2pubchunk` has the following prototype:

        int abcrb_prv2pubchunk(
                void *ctx,
                unsigned char *pubchunk,
                const unsigned char *_prv,
                unsigned chunkid);

Although the parameter `pubchunk` has the type `unsigned char *` but in fact
the implementation for this function implicitly impose a stricter alignment
requirement for the referenced object: the referenced object must be 4 bytes
aligned.  We should change the implementation to lessen the requirement or
just update the type to `uint32_t *`.

### 需要 Automated Test 拿原版 Rainbow (without changes / with minimal changes) 來對答案

用來快速開發迭代

### Explicit dependency on MBEDTLS library: SHA-256 and AES-256 and AES-256-CTR-DRBG

Only need a thin API glueing layer

How we should use DRBG?

### Change seed length to variable between 16 bytes and 64 bytes (而不是固定的 32 bytes)

Add one argument for variable length of seed `unsigned seedlen`

原本 compressed private key 的定義是 seed 加上 12 bytes (little-endian `uint32_t[3]`)

若修改 seed 為變動長度，則 compressed private key 也會是變動長度的

*或者考慮：總是使用 SHA-256 把任意長度的 seed 轉成 32-byte 的值，再使用，這樣子結構比較簡單。*

### Consider exposing an `abcrb_config.h` file

設定，例如 `max_num_attempts_prng` 等

### add prototype for signature verification subroutine in the main API header file

currently in `rainbow_16.h` we have
```
int rainbow_verify(const uint8_t *digest, const uint8_t *signature, const uint8_t *pk);
```

### 做一個 verify only sub library 給 bootloader 用

### Working buffer

Todo: We may choose to just use internal buffers instead of thoses provided by
caller from the outside world.

Because the target use case is that at most one operation can be performed at
the same time, and all operation is stateless, we can simply declare a union
object with internal linkage for working buffers of all supported operations.

### Remove unused code

For example, `prng_bytes`

### Do not define NDEBUG

### Do not use assert

### External header (APIs exposed to library user) vs internal header (private interfaces exposed to internal modules)

### Refactor so that we can reliably & easily swap out the PRNG?

### Rename this library

This deterministic Rainbow implementation is also usable outside the scope of
ABCMint.  Thus we may want to name it in a different way.

DRB = Deterministic RainBow
DRBIA = Deterministic RainBow parameter set IA
DRB16 = Deterministic RainBow 16
DRB16323232 = Deterministic RainBow(GF(16),32,32,32)

### No need to consider C++

Just C99.  Don't think about C++.  No need to extern "C".

### Pointer safety issue

Make sure the publicly exposed library functions can accept buffer of no alignment requirement!

所有 API entropy point 都先把資料 memcpy 塞進 `uint32_t[]` buffer 再開始處理，就能消除掉大部分的 alignment 問題

For all pointers we must specify
    - Nullability
    - Alignment
    - Aliasing
    - Boundary if referencing an array element instead of just an object
    - The semantics

### 釐清: 目前實作裡面出現了很多 `unsigned` 型別，有哪些可以轉換成 `uint32_t` 而不影響正確性呢？ (提升 portability)

### Consider adding `__attribute__((warn_unused_result))` function attribute to generate warning if return value unused

### Consider only little-endian 32-bit reference implementation

### NIST Standard Conformance

- 我們的 deterministic cryptographic key generation 機制是基於
    - NIST SP 800-90A Rev. 1 CTR DRBG
        ```
        block cipher set to AES-256
        counter length set to the block length 128 bits
        with derivation function
        without prediction resistance
        requested instantiation security strength set to 256 bits
        ```
- 我們的 deterministic cryptographic key generation 兩個應用:
    - 應用 A: 從 account seed 生成 account (compressed) private key
        ```
        entropy_input           = 256 bits hash of secret seed
        nonce                   = empty
        personalization_string  = empty
        ```
    - 應用 B: 生成數位簽章
        ```
        entropy_input           = 256 bits hash of secret seed
        nonce                   = 256 bits hash of message
        personalization_string  = empty
        ```
- 注意標準説 `seed_material = entropy_input || nonce || personalization_string`
    - 最後總是會把 bit string 串接在一起
    - 在已知這個事實的情況下 `entropy_input`, `nonce`, `personalization_string` 三個值的定義可以比較彈性
        - 也許某個函式庫實作只暴露出特定的 API 但我們仍然能湊出想要的效果

How to use DRBG?  Refer to https://tools.ietf.org/html/rfc6979

### 需要確認, 對於目前微調後的 Rainbow 實作, 我們有辦法撰寫乾淨的 formal spec

本系統必須要能夠寫 deterministic key gen 的 formal spec 不可以有模糊地帶

- 先確認程式碼已經整理成之後方便寫 formal spec 演算法的樣子
- 我們的 `rainbow_sign()` algorithm 7 (和 Rainbow NIST submission 有些微差異)
- 引入 deterministic 機制

### CTR DRBG 部分，找現成的函式庫對答案

拿其他的 AES 256 CTR DRBG 實作來對答案

* 參考實作一 OpenSSL 1.1.1
    * crypto/rand/rand_lcl.h
    * crypto/rand/drbg_ctr.c
    * crypto/rand/drbg_lib.c
    * OpenSSL Download for 1.1.1 Release (openssl-1.1.1.tar.gz) https://www.openssl.org/source/openssl-1.1.1.tar.gz
    * OpenSSL GitHub git repo tag "OpenSSL_1_1_1"
    * OpenSSL GitHub git repo branch "OpenSSL_1_1_1-stable"
    * https://www.openssl.org/source/
    * https://www.openssl.org/news/newslog.html
    * https://www.openssl.org/news/changelog.html
    * https://www.openssl.org/news/vulnerabilities.html
    * https://www.openssl.org/blog/blog/2018/09/11/release111/
    * https://en.wikipedia.org/wiki/OpenSSL
    * https://github.com/openssl/openssl/tree/OpenSSL_1_1_1-stable

* 參考實作二 mbedTLS
    * https://tls.mbed.org/tech-updates/releases/mbedtls-2.12.0-2.7.5-and-2.1.14-released
    * https://tls.mbed.org/tech-updates/releases/mbedtls-2.13.0-2.7.6-and-2.1.15-released
    * https://tls.mbed.org/tech-updates/releases/mbedtls-2.14.0-2.7.7-and-2.1.16-released

* 參考實作三 rng.c rng.h copyrighted by Lawrence E. Bassham
    * https://github.com/Microsoft/PQCrypto-SIKE
    * https://github.com/dconnolly/NIST-PQC-SIKE
    * https://github.com/WardBeullens/Nutcracker

* 參考實作四 Bouncy Castle Java Distribution
    * https://github.com/bcgit/bc-java
    * https://github.com/netroby/jdk9-dev/blob/master/jdk/src/java.base/share/classes/sun/security/provider/CtrDrbg.java

* 參考其他
    https://elixir.bootlin.com/linux/v4.19/source/crypto/drbg.c
    https://elixir.bootlin.com/linux/v4.19/source/include/crypto/drbg.h
    https://github.com/torvalds/linux/blob/v4.19/include/crypto/drbg.h


### clean up the struct definition for the following three functions

```
int abcrb_getpubkey_clear(abcrb_getpubkey_ctx_t *ctx);
int abcrb_getpubkey_prepare(abcrb_getpubkey_ctx_t *ctx, const uint8_t *prv);
int abcrb_getpubkey_gen(abcrb_getpubkey_ctx_t *ctx, uint8_t *pubchunk, const uint8_t *prv, int chunkid);
```

### 應該要把 PRNG 的 API 改成會回傳 error codes 因為他們有可能會失敗！！！！

```
int prng_setup(void *prng_ctx, const void *seed, unsigned seedlen);
int prng_gen(void *prng_ctx, void *out, unsigned numbytes);
```

### 將 mbedTLS 改成最新的 long-term support branch 的發行版本 2.7.7

三個版本之間 2.7.5 / 2.7.6 / 2.7.7 我們需要用的部分有改變嗎？

### Other notes on `CTR_DRBG` by @js

請看手上的 1-rainbow-refactor.txt 筆記
