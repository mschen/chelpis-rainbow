CC = gcc
CFLAGS = -std=c99 -Wall -Wextra -Wpedantic -g -O2 -fsanitize=address
LIB_SRC_FILES = $(wildcard src/*.c)
LIB_DEP_FILES = $(wildcard src/*.c src/*.h)
EXECUTABLES = main

.PHONY: all clean fmt

all: $(EXECUTABLES)

clean:
	$(RM) $(EXECUTABLES)

fmt:
	find src -type f -iname '*.[ch]' -exec clang-format -style=file -i {} +

main: main.c $(LIB_DEP_FILES)
	$(CC) $(CFLAGS) main.c $(LIB_SRC_FILES) -o main
