#ifndef DRB_DEPS_H_INCLUDED
#define DRB_DEPS_H_INCLUDED

// stateful prng interface
void drb_prng_init(const unsigned char *seed, unsigned long seedlen);
void drb_prng_generate(unsigned char *out, unsigned long numbytes);
void drb_prng_free(void);

// stateless sha256 interface
void drb_sha256(unsigned char *dgst, const unsigned char *msg, unsigned long msglen);

#endif  // DRB_DEPS_H_INCLUDED
