#ifndef DRB_API_H_INCLUDED
#define DRB_API_H_INCLUDED

enum {
    DRB_ERROR_NONE,
    DRB_ERROR_BAD_SIGNATURE,
    DRB_ERROR_BAD_INPUT,
    DRB_ERROR_PRNG_MAX_NUM_ATTEMPTS_REACHED,
    DRB_ERROR_OTHER_REASON,
};

#define DRB_NUM_BYTES_DIGEST 32
#define DRB_NUM_BYTES_SIGNATURE 64
#define DRB_NUM_BYTES_PUBLIC_KEY 152097
#define DRB_NUM_BYTES_PUBLIC_KEY_CHUNK 4096
#define DRB_NUM_CHUNKS_PUBLIC_KEY 38
#define DRB_NUM_BYTES_SECRET_SEED 32
#define DRB_NUM_BYTES_COMPRESSED_PRIVATE_KEY 44

int drb_secret_seed_to_compressed_private_key(
        unsigned char *compressed_private_key,
        const unsigned char *secret_seed);

int drb_compressed_private_key_to_one_public_key_chunk(
        unsigned char *public_key_chunk,
        const unsigned char *compressed_private_key,
        unsigned chunk_id);

int drb_reorder_public_key(
        unsigned char *public_key,
        const unsigned char *all_public_key_chunks);

int drb_sign(
        unsigned char *signature,
        const unsigned char *compressed_private_key,
        const unsigned char *message_digest);

int drb_verify(
        const unsigned char *public_key,
        const unsigned char *signature,
        const unsigned char *message_digest);

#endif  // DRB_API_H_INCLUDED
