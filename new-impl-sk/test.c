#include "drb12816/drb12816.h"
#include "external/external.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//////////////////////  for integrated test  //////////////////////////////////////

#include "drb12816/rainbow_config.h"
#include "drb12816/mpkc.h"

int drb12816_verify(const unsigned char *pubkey, const unsigned char *digest, const unsigned char *signature) {
    uint8_t digest_ck[_PUB_M_BYTE];
    //rainbow_pubmap(digest_ck, pk, signature);
    gf16mpkc_mq_eval_n_m(digest_ck, pubkey, signature, _PUB_N, _PUB_M);

    uint8_t correct[_PUB_M_BYTE];
    uint8_t digest_salt[_HASH_LEN + _SALT_BYTE];
    memcpy(digest_salt, digest, _HASH_LEN);
    memcpy(digest_salt + _HASH_LEN, signature + _PUB_N_BYTE, _SALT_BYTE);
    //sha2_chain_msg(correct, _PUB_M_BYTE, digest_salt, _HASH_LEN + _SALT_BYTE);
    sha256(correct, digest_salt, _HASH_LEN + _SALT_BYTE);

    uint8_t cc = 0;
    for (unsigned i = 0; i < _PUB_M_BYTE; i++) {
        cc |= (digest_ck[i] ^ correct[i]);
    }
    return (0 == cc) ? 0 : -1;
}

//////////////////////  for integrated test  //////////////////////////////////////



#define HALT_IF(err) _halt_if_(__FILE__, __LINE__, #err, (err))
static void _halt_if_(const char *file, int line, const char *err_expr, int err) {
    if (!err) return;
    printf("Nonzero error code in %s line %d:\t%s ==> %d\n", file, line, err_expr, err);
    printf("Program stopped\n");
    exit(1);
}

#define HEXDUMP(ptr, len) _hexdump_(__FILE__, __LINE__, #ptr, (ptr), (len))
static void _hexdump_(const char *file, int line, const char *ptr_expr, const void *ptr, unsigned len) {
    printf("Hexdump in %s line %d:\t%s ==> [%u bytes] [", file, line, ptr_expr, len);
    for (unsigned i = 0; i < len; ++i) {
        printf("%02x", ((const unsigned char *) ptr)[i]);
    }
    printf("]\n");
}

void test_0(void) {
    printf("-----BEGIN TEST 0-----\n");
    {
        printf("computing SHA-256 of empty byte string\n");
        unsigned char dgst[32] = {0};
        int err = sha256(dgst, 0, 0);
        HALT_IF(err);
        HEXDUMP(dgst, 32);
    }

    {
        printf("computing SHA-256 of 2-byte data \"hi\"\n");
        unsigned char dgst[32] = {0};
        int err = sha256(dgst, (const unsigned char *) "hi", 2);
        HALT_IF(err);
        HEXDUMP(dgst, 32);
    }

    {
        printf("generating a 2500-byte pseudorandom byte string from a 64-byte seed\n");

        const void *const SEED = "\x61\xf3\x2c\xd3\x08\x31\x17\xab\x3a\xd0\x67\xbf\x12\x5c\xed\x76\x4a\x28\x80\xc4\x0a\xa1\x9b\x9e\x8d\x2c\xde\xbf\x27\xfb\xe6\xed\xec\xc2\x9d\xe5\x7e\x69\x78\xc5\xe9\xd6\x25\xdb\xca\x7f\x0e\x6d\x11\xbd\xcb\xd7\xf2\x79\x9f\xf0\x56\x5d\x2e\x8d\x19\x87\x32\x1a";
        const unsigned SEEDLEN = 64;
        struct prng_context my_prng_ctx = {0};
        unsigned char output[2500] = {0};
        unsigned char dgst[32] = {0};
        int err = 0;

        // instantiate
        err = prng_begin(&my_prng_ctx, SEED, SEEDLEN);
        HALT_IF(err);

        // generate pseudo-random bytes
        err = prng_gen(&my_prng_ctx, output, 7);
        HALT_IF(err);
        err = prng_gen(&my_prng_ctx, output + 7, 25);
        HALT_IF(err);
        err = prng_gen(&my_prng_ctx, output + 32, 2016);
        HALT_IF(err);
        err = prng_gen(&my_prng_ctx, output + 2048, 452);
        HALT_IF(err);

        // uninstantiate
        prng_end(&my_prng_ctx);

        // sha256
        printf("computing SHA-256 digest of the 2500-byte data\n");
        err = sha256(dgst, output, 2500);
        HALT_IF(err);

        // dump
        HEXDUMP(dgst, 32);
        HALT_IF(0 != memcmp(dgst, "\x15\x8b\x60\x57\xd4\xa1\x12\xda\x32\x4a\xa6\x83\x72\x07\x7a\xe6\x73\x85\xb0\xbf\x54\xc0\x30\x53\x67\x54\xca\xeb\xd1\xef\xe3\x52", 32));
    }

    printf("-----END TEST 0-----\n");
}

void test_1(void) {
    printf("-----BEGIN TEST 1-----\n");

    unsigned char sig[64] = {0};

    const unsigned char seed[44] = {0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0x50, 0x1a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x08, 0x00, 0x00};
    const unsigned seedlen = 44;
    const unsigned char dgst[32] = {0xe7, 0x5b, 0x10, 0x13, 0x0b, 0x0f, 0x1d, 0x4d, 0x81, 0xd7, 0xef, 0x0d, 0x02, 0xc3, 0xd8, 0xa4, 0xf8, 0x3c, 0x2a, 0x48, 0xef, 0x34, 0xcd, 0xf1, 0x6f, 0x70, 0x3b, 0x1d, 0x2a, 0x21, 0xf4, 0x22};

    int err = drb12816_sign(sig, seed, seedlen, dgst);

    HALT_IF(err);
    HEXDUMP(sig, 64);

    printf("-----END TEST 1-----\n");
}

void test_2(void) {
    printf("-----BEGIN TEST 2-----\n");

    unsigned char pubchunk[4096] = {0};

    const unsigned char seed[44] = {0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0x50, 0x1a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x08, 0x00, 0x00};
    const unsigned seedlen = 44;
    const unsigned chunkid = 37;

    int err = drb12816_genpubchunk(pubchunk, seed, seedlen, chunkid);

    HALT_IF(err);
    HEXDUMP(pubchunk, 4096);

    printf("-----END TEST 2-----\n");
}



void test_3(void) {
    printf("-----BEGIN TEST 3-----\n");

    unsigned char sig[64] = {0};

    const unsigned char seed[44] = {0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0x50, 0x1a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x08, 0x00, 0x00};
    const unsigned seedlen = 44;
    const unsigned char dgst[32] = {0xe7, 0x5b, 0x10, 0x13, 0x0b, 0x0f, 0x1d, 0x4d, 0x81, 0xd7, 0xef, 0x0d, 0x02, 0xc3, 0xd8, 0xa4, 0xf8, 0x3c, 0x2a, 0x48, 0xef, 0x34, 0xcd, 0xf1, 0x6f, 0x70, 0x3b, 0x1d, 0x2a, 0x21, 0xf4, 0x22};

    int err = drb12816_sign(sig, seed, seedlen, dgst);

    HALT_IF(err);
    HEXDUMP(sig, 64);

    unsigned char pk[4096*38];
    for(unsigned i=0;i<38;i++) {
        printf("genchunk: %d...\n",i);
        err = drb12816_genpubchunk(pk + i*4096, seed, seedlen, i);
        HALT_IF(err);
    }
    err = drb12816_verify( pk , dgst , sig );
    HALT_IF(err);
    printf("test passed.\n");

    printf("-----END TEST 3-----\n");
}



int main(void) {
    test_0();
    test_1();
    test_2();
    test_3();
    return 0;
}
