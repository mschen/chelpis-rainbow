#include "external.h"

#include "mbedtls/ctr_drbg.h"
#include "mbedtls/sha256.h"

#include <stddef.h>
#include <string.h>

static int _dummy_f_entropy(void *p_entropy, unsigned char *out, size_t outlen) {

    // The type of the 2nd argument of mbedtls_ctr_drbg_seed_entropy_len() is:
    //
    //      int (*)(void *, unsigned char *, size_t)
    //
    // This is just a dummy implementation because for our deterministic use
    // case we never access external entropy source.
    (void) p_entropy;
    (void) out;

    return (outlen != 0) ? -1 : 0;
}

int prng_begin(struct prng_context *ctx, const void *prng_seed, unsigned long prng_seedlen) {

    // Reject bad inputs.
    if (!(ctx && (prng_seedlen == 0 || (prng_seed && prng_seedlen <= PRNG_MAX_SEED_LEN)))) return -1;

    // Zeroize and initialize the struct members.
    memset(ctx, 0, sizeof (struct prng_context));
    mbedtls_ctr_drbg_init(&ctx->ctr_drbg_ctx);

    // We call the "internal" function mbedtls_ctr_drbg_seed_entropy_len()
    // instead of mbedtls_ctr_drbg_seed() because we want to control the
    // number of bytes requested from external entropy source.  Note that the
    // default value for MBEDTLS_CTR_DRBG_RESEED_INTERVAL is 10000 and in our
    // use case the value of ctx->ctr_drbg_ctx.reseed_counter never exceeds
    // 1024 when mbedtls_ctr_drbg_random_with_add() is called, so
    // mbedtls_ctr_drbg_reseed() is called once for this instantiation here.
    int err = mbedtls_ctr_drbg_seed_entropy_len(&ctx->ctr_drbg_ctx, _dummy_f_entropy, 0, prng_seed, prng_seedlen, 0);

    // Check the returned error code.
    if (err) return -2;

    // Report success.
    return 0;
}

int prng_gen(struct prng_context *ctx, unsigned char *out, unsigned long outlen) {

    // Reject bad inputs.
    if (!(
        ctx
        && ctx->totlen <= PRNG_MAX_NUM_BYTES_GEN
        && ctx->remlen <= PRNG_INTERNAL_BUF_SIZE
        && (outlen == 0 || out)
        && outlen < PRNG_MAX_NUM_BYTES_GEN
        && outlen + ctx->totlen < PRNG_MAX_NUM_BYTES_GEN
    )) return -1;

    while (outlen) {
        // Ask CTR DRBG for more bytes, if no remaining bytes in the internal buffer.
        if (ctx->remlen == 0) {
            int err = mbedtls_ctr_drbg_random(&ctx->ctr_drbg_ctx, ctx->buf, PRNG_INTERNAL_BUF_SIZE);
            if (err) return -2;
            ctx->remlen = PRNG_INTERNAL_BUF_SIZE;
        }

        // Read nb random bytes from internal buffer.
        unsigned long nb = outlen > ctx->remlen ? ctx->remlen : outlen;
        memcpy(out, &ctx->buf[PRNG_INTERNAL_BUF_SIZE - ctx->remlen], nb);

        // Update pointers and counters to reflect that we just read nb bytes.
        out += nb;
        outlen -= nb;
        ctx->totlen += nb;
        ctx->remlen -= nb;
    }

    return 0;
}

void prng_end(struct prng_context *ctx) {
    if (!ctx) return;

    // Clean up and zeroize everything.
    mbedtls_ctr_drbg_free(&ctx->ctr_drbg_ctx);
    memset(ctx, 0, sizeof (struct prng_context));
}

int sha256(unsigned char *dgst, const unsigned char *msg, unsigned long msglen) {
    int err = mbedtls_sha256_ret(msg, msglen, dgst, 0);
    if (err) return -1;
    return 0;
}
