#ifndef EXTERNAL_H_INCLUDED
#define EXTERNAL_H_INCLUDED

#include "mbedtls/ctr_drbg.h"

#define PRNG_MAX_SEED_LEN 256
#define PRNG_MAX_NUM_BYTES_GEN (1UL * 1024 * 1024)
#define PRNG_INTERNAL_BUF_SIZE 1024

struct prng_context {
    unsigned long totlen;
    unsigned long remlen;
    unsigned char buf[PRNG_INTERNAL_BUF_SIZE];
    mbedtls_ctr_drbg_context ctr_drbg_ctx;
};

int prng_begin(struct prng_context *ctx, const void *prng_seed, unsigned long prng_seedlen);
int prng_gen(struct prng_context *ctx, unsigned char *out, unsigned long outlen);
void prng_end(struct prng_context *ctx);

int sha256(unsigned char *dgst, const unsigned char *msg, unsigned long msglen);

#endif
