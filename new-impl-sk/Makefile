CC = gcc

CFLAGS = -std=c11
CFLAGS += -O0
CFLAGS += -g
CFLAGS += -Wall -Wextra -Wpedantic
CFLAGS += -fsanitize=address

# Source files of the library
LIB_SRC_FILES = $(wildcard drb12816/*.c)
LIB_DEP_FILES = $(wildcard drb12816/*.c drb12816/*.h)

# Add external dependencies (prng and sha256 based on mbedTLS)
CFLAGS += -I./external/include/
LIB_SRC_FILES += $(wildcard external/*.c external/library/*.c)
LIB_DEP_FILES += $(wildcard external/*.c external/library/*.c external/*.h external/include/mbedtls/*.h)

.PHONY: all
all: test

.PHONY: clean
clean:
	rm -rf test test.dSYM

.PHONY: fmt
fmt:
	find drb12816 -type f -iname '*.[ch]' -exec clang-format -style=file -i {} +

test: test.c $(LIB_DEP_FILES)
	$(CC) $(CFLAGS) test.c $(LIB_SRC_FILES) -o test
