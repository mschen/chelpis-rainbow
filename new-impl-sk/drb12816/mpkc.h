#ifndef _MPKC_H_
#define _MPKC_H_

#include <stdint.h>

void gf16mpkc_mq_eval_n_m(uint8_t *z, const uint8_t *pk_mat, const uint8_t *w, unsigned n, unsigned m);

#endif
