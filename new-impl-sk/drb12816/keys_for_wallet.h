#ifndef _WALLET_KEY_H_
#define _WALLET_KEY_H_

#define ABCRB_CONFIG_MAX_ATTEMP_PRNG 100


#define HASH_LEN_FOR_KEYS 32
#define SEED_LEN_FOR_KEYS  HASH_LEN_FOR_KEYS


#include "rainbow_config.h"

#include <stdalign.h>
#include <stdint.h>

///
/// The secret key of original nist-rainbow submission
///
typedef struct {
    alignas(4) uint8_t l1_o[_O1 * _O1_BYTE];
    alignas(4) uint8_t l1_vo[_O1][_V1 * _O1_BYTE];
    alignas(4) uint8_t l1_vv[TERMS_QUAD_POLY(_V1) * _O1_BYTE];

    alignas(4) uint8_t l2_o[_O2 * _O2_BYTE];
    alignas(4) uint8_t l2_vo[_O2][_V2 * _O2_BYTE];
    alignas(4) uint8_t l2_vv[TERMS_QUAD_POLY(_V2) * _O2_BYTE];
} ns_rainbow_ckey;

typedef struct {
    alignas(4) uint8_t mat_t[_PUB_N * _PUB_N_BYTE];
    alignas(4) uint8_t vec_t[_PUB_N_BYTE];
    alignas(4) uint8_t mat_s[_PUB_M * _PUB_M_BYTE];
    alignas(4) uint8_t vec_s[_PUB_M_BYTE];

    ns_rainbow_ckey ckey;
} ns_rainbow_key;

///
/// Division of the ckey
///
typedef struct {
    alignas(4) uint8_t l1_o[_O1 * _O1_BYTE];
    alignas(4) uint8_t l1_vo[_O1][_V1 * _O1_BYTE];
    alignas(4) uint8_t l1_vv[TERMS_QUAD_POLY(_V1) * _O1_BYTE];
} ckey_l1;

/// sizeof(ckey_l1)
#define SIZE_CKEY_L1 (25872)

typedef struct {
    alignas(4) uint8_t l2_o[_O2 * _O2_BYTE];
    alignas(4) uint8_t l2_vo[_O2][_V2 * _O2_BYTE];
} ckey_l2_o_vo;

/// sizeof(ckey_l2_o_vo)
#define SIZE_CKEY_L2_O_VO (33280)

typedef struct {
    alignas(4) uint8_t l2_vv[TERMS_QUAD_POLY(_V2) * _O2_BYTE];
} ckey_l2_vv;

/// sizeof(ckey_l2_vv)
#define SIZE_CKEY_L2_VV (34320)



///
///  The secret key format for abcrb
///
typedef struct {
    alignas(4) uint8_t seed[SEED_LEN_FOR_KEYS];
    uint32_t prng_idx_c;
    uint32_t prng_idx_s;
    uint32_t prng_idx_t;
} abcrb_secretkey_t;


int abcrb_seed2prv(abcrb_secretkey_t *abcrb_prv, const uint8_t *seed, unsigned seedlen);

int gen_pubkey_chunk(uint8_t *pubchunk, const abcrb_secretkey_t *prv, int chunkid);

/////////////////

void _abcrb_expand_secretkey(ns_rainbow_key *sk, const abcrb_secretkey_t *prv);

/////////////////


// This header file provides:
//      the definition for struct prng_context
//      the prototype for function prng_begin()
//      the prototype for function prng_gen()
//      the prototype for function prng_end()
//      the prototype for function sha256()
#include "../external/external.h"


///
/// return value:
/// positive value: length of bytes aquired form the prng
/// negative value: errors.
///
int gf16mat_rand_fullrank_mat(struct prng_context * prng_ctx, uint8_t *mat, unsigned dim);

int gf16mat_rand_fullrank_mat2(struct prng_context *saved_prng_state, struct prng_context *prng_ctx, uint8_t *mat, unsigned dim);

int gf16mat_rand_fullrank_invmat(struct prng_context *prng_ctx, uint8_t *mat, unsigned dim);


unsigned extract_s_map(struct prng_context *prng_ctx, unsigned prng_idx, uint8_t *mat_s, uint8_t *vec_s, const abcrb_secretkey_t *prv);

unsigned extract_t_map(struct prng_context *prng_ctx, unsigned prng_idx, uint8_t *mat_t, uint8_t *vec_t, const abcrb_secretkey_t *prv);

unsigned extract_invs_map(struct prng_context *prng_ctx, unsigned prng_idx, uint8_t *mat_s, uint8_t *vec_s, const abcrb_secretkey_t *prv);

unsigned extract_invt_map(struct prng_context *prng_ctx, unsigned prng_idx, uint8_t *mat_t, uint8_t *vec_t, const abcrb_secretkey_t *prv);

#endif
