#ifndef DRB12816_INTERNAL_H_INCLUDED
#define DRB12816_INTERNAL_H_INCLUDED

#define DRB12816_MAX_RNG_ATTEMPS 100


int abcrb_sign(uint8_t *signature, const abcrb_secretkey_t * prv, const uint8_t *_digest);


#endif
