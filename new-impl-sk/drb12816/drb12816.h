#ifndef DRB12816_H_INCLUDED
#define DRB12816_H_INCLUDED

#define DRB12816_BYTELEN_SEED_MIN 16
#define DRB12816_BYTELEN_SEED_MAX 64
#define DRB12816_BYTELEN_DGST 32
#define DRB12816_BYTELEN_SIG 64
#define DRB12816_NUM_PUBCHUNKS 38
#define DRB12816_BYTELEN_PUBCHUNK 4096
#define DRB12816_BYTELEN_PUB 152097

#define DRB12816_ERROR_NONE 0
#define DRB12816_ERROR_BAD_INPUT (-1)
#define DRB12816_ERROR_BAD_RNG (-2)
#define DRB12816_ERROR_OTHER_REASON (-3)
#define DRB12816_ERROR_NO_FULLRANK_MAT (-4)

int drb12816_sign(unsigned char *sig, const unsigned char *seed, unsigned seedlen, const unsigned char *dgst);

int drb12816_genpubchunk(unsigned char *pubchunk, const unsigned char *seed, unsigned seedlen, unsigned chunkid);

#endif
