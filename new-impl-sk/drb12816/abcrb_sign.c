#include <stdalign.h>
#include <string.h>


#include "rainbow_config.h"
#include "keys_for_wallet.h"

#include "blas.h"
#include "mpkc.h"


// This header file provides:
//      the definition for struct prng_context
//      the prototype for function prng_begin()
//      the prototype for function prng_gen()
//      the prototype for function prng_end()
//      the prototype for function sha256()
#include "../external/external.h"

#include "drb12816.h"

////////////////////////////////

#define MAX_ATTEMPT_SIGN 128

/////////////////////////////



static void gen_l1_mat(uint8_t *mat, const ckey_l1 *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O1; i++) {
        gf16mat_prod(mat + i * _O1_BYTE, k->l1_vo[i], _O1_BYTE, _V1, v);
    }
    gf256v_add(mat, k->l1_o, _O1_BYTE * _O1);
}

static void gen_l2_mat(uint8_t *mat, const ckey_l2_o_vo *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O2; i++) {
        gf16mat_prod(mat + i * _O2_BYTE, k->l2_vo[i], _O2_BYTE, _V2, v);
    }
    gf256v_add(mat, k->l2_o, _O2_BYTE * _O1);
}

static unsigned linear_solver_32x32(uint8_t *buffer_for_gaussian, uint8_t *r, const uint8_t *mat_32x32, const uint8_t *cc) {
    uint8_t *mat = buffer_for_gaussian;
    for (unsigned i = 0; i < _O1; i++) {
        memcpy(mat + i * (_O1_BYTE + 1), mat_32x32 + i * (_O1_BYTE), _O1_BYTE);
        mat[i * (_O1_BYTE + 1) + _O1_BYTE] = gf16v_get_ele(cc, i);
    }
    unsigned r8 = gf16mat_gauss_elim(mat, _O1, _O1 + 2);
    for (unsigned i = 0; i < _O1; i++) {
        gf16v_set_ele(r, i, mat[i * (_O1_BYTE + 1) + _O1_BYTE]);
    }
    return r8;
}



///////////////////////////////////////////////////////


/// algorithm 7
/// modified from : int rainbow_sign( uint8_t * signature , const uint8_t * _sk , const uint8_t * _digest )
int abcrb_sign(uint8_t *signature, const abcrb_secretkey_t * prv, const uint8_t *_digest) {

    /// initialize prng for secret key
    struct prng_context _prng_key;
    struct prng_context * prng_key = (struct prng_context *) &_prng_key;
    prng_begin(prng_key, prv->seed, SEED_LEN_FOR_KEYS);
    unsigned prng_idx = 0;

    /// initialize prng for signing
    struct prng_context _prng_sign;
    struct prng_context * prng_sign = (struct prng_context *) &_prng_sign;
    uint8_t pre_prng_seed[ SEED_LEN_FOR_KEYS + DRB12816_BYTELEN_DGST ];
    memcpy(pre_prng_seed, prv->seed, SEED_LEN_FOR_KEYS );
    memcpy(pre_prng_seed + SEED_LEN_FOR_KEYS, _digest, DRB12816_BYTELEN_DGST );
    alignas(4) uint8_t prng_seed[ 32 ];
    sha256(prng_seed, pre_prng_seed, SEED_LEN_FOR_KEYS + DRB12816_BYTELEN_DGST );
    prng_begin(prng_sign, prng_seed, 32 );

    uint32_t _mat_96x96[1152];   /// (96*96/2)/4
    uint8_t *mat_96x96 = (uint8_t*) &_mat_96x96;
    uint32_t _vec_96[12];   /// (96/2/4)
    uint8_t *vec_96 = (uint8_t*) &_vec_96;
    uint8_t *mat_s = mat_96x96;
    uint8_t *mat_t = mat_96x96;
    uint8_t *vec_s = vec_96;
    uint8_t *vec_t = vec_96;

    uint32_t _buffer_for_gaussian[256];  /// 32x32
    uint8_t *buffer_for_gaussian = (uint8_t*) &_buffer_for_gaussian[0];

    uint32_t _mat_32x32[256];   /// (32*32)/4
    uint8_t *mat_l1 = (uint8_t*) &_mat_32x32[0];
    uint8_t res_l1_vv[_O1_BYTE];

    /// presuming: sizeof( ckey_l2_vv ) > sizeof( ckey_l2_o_vo )  > sizeof( ckey_l1 )
    ckey_l2_vv sto_ckey;
    ckey_l1 *k_l1 = (ckey_l1 *) &sto_ckey;

    /// generate S map
    prng_idx = extract_invs_map(prng_key, prng_idx, mat_s, vec_s, prv);
    /// dispose T map
    prng_idx = extract_t_map(prng_key, prng_idx, (uint8_t*)&sto_ckey, buffer_for_gaussian, prv);
    /// generate cmap_l1
    prng_gen(prng_key, (uint8_t*)k_l1, sizeof(ckey_l1));  // XXX

    /// roll vinegars.
    alignas(4) uint8_t vinegar[_V1_BYTE];
    unsigned l1_succ = 0;
    unsigned time = 0;
    while (!l1_succ) {
        if (MAX_ATTEMPT_SIGN == time) 
            break;
        prng_gen(prng_sign, vinegar, _V1_BYTE);
        gen_l1_mat(mat_l1, k_l1, vinegar);

        // make sure full-ranked mat_l1
        memcpy(buffer_for_gaussian, mat_l1, (_O1) * (_O1_BYTE));
        l1_succ = gf16mat_gauss_elim(buffer_for_gaussian, _O1, _O1);
        time++;
    }
    gf16mpkc_mq_eval_n_m(res_l1_vv, k_l1->l1_vv, vinegar, _V1, _O1);

    /// main work
    uint32_t _mat_l2[256];   /// (32*32)/4
    uint8_t *mat_l2 = (uint8_t*) &_mat_l2[0];

    struct prng_context _prng_l2;
    struct prng_context * prng_for_l2 = (struct prng_context *) &_prng_l2;
    memcpy(prng_for_l2, prng_key, sizeof(struct prng_context) ); /// save the state of the prng for gernerating l2-ckey

    uint8_t *key_l2 = (uint8_t*)&sto_ckey;

    alignas(4) uint8_t temp_o1[_O1_BYTE] = {0};
    alignas(4) uint8_t temp_o2[_O2_BYTE];
    //// line 7 - 14
    alignas(4) uint8_t _z[_PUB_M_BYTE];
    alignas(4) uint8_t y[_PUB_M_BYTE];
    alignas(4) uint8_t x[_PUB_N_BYTE];
    alignas(4) uint8_t w[_PUB_N_BYTE];

    alignas(4) uint8_t digest_salt[DRB12816_BYTELEN_DGST + _SALT_BYTE] = {0};
    memcpy(digest_salt, _digest, DRB12816_BYTELEN_DGST);
    uint8_t *salt = digest_salt + DRB12816_BYTELEN_DGST;  // XXX align

    memcpy(x, vinegar, _V1_BYTE);
    unsigned succ = 0;
    unsigned my_time = time;
    while (!succ) {
        if (MAX_ATTEMPT_SIGN == time) {
            break;
        }
        prng_gen(prng_sign, salt, _SALT_BYTE);
        ///  sha2_chain_msg(_z, _PUB_M_BYTE, digest_salt, _HASH_LEN + _SALT_BYTE);  /// line 9
        sha256(_z, digest_salt, DRB12816_BYTELEN_DGST + _SALT_BYTE ); /// required: (_PUB_M_BYTE == 32) which is the same by coincidence.

        gf256v_add(_z, vec_s, _PUB_M_BYTE);
        gf16mat_prod(y, mat_s, _PUB_M_BYTE, _PUB_M, _z);  /// line 10

        memcpy(temp_o1, res_l1_vv, _O1_BYTE);
        gf256v_add(temp_o1, y, _O1_BYTE);
        linear_solver_32x32(buffer_for_gaussian, x + _V1_BYTE, mat_l1, temp_o1);

        if(my_time!=time) {
            memcpy(prng_key, prng_for_l2, sizeof(struct prng_context) );  /// rewide prng
        }
        prng_gen(prng_key, key_l2, sizeof(ckey_l2_o_vo));  // XXX
        ckey_l2_o_vo *k_l2_o_vo = (ckey_l2_o_vo *) key_l2;

        gen_l2_mat(mat_l2, k_l2_o_vo, x);

        prng_gen(prng_key, key_l2, sizeof(ckey_l2_vv));  // XXX
        ckey_l2_vv *k_l2_vv = (ckey_l2_vv *) key_l2;

        gf16mpkc_mq_eval_n_m(temp_o2, k_l2_vv->l2_vv, x, _V2, _O2);
        gf256v_add(temp_o2, y + _O1_BYTE, _O2_BYTE);
        succ = linear_solver_32x32(buffer_for_gaussian, x + _V2_BYTE, mat_l2, temp_o2);  /// line 13

        time++;
    };

    /// rewind prng
    prng_begin(prng_key, prv->seed, SEED_LEN_FOR_KEYS);
    prng_idx = 0;

    prng_idx = extract_s_map(prng_key, prng_idx, mat_t, vec_t, prv);
    prng_idx = extract_invt_map(prng_key, prng_idx, mat_t, vec_t, prv);

    gf256v_add(x, vec_t, _PUB_N_BYTE);
    gf16mat_prod(w, mat_t, _PUB_N_BYTE, _PUB_N, x);

    memset(signature, 0, _SIGNATURE_BYTE);

    gf256v_add(signature, w, _PUB_N_BYTE);
    gf256v_add(signature + _PUB_N_BYTE, salt, _SALT_BYTE);

    prng_end( prng_key );
    prng_end( prng_sign );
    if (MAX_ATTEMPT_SIGN <= time)
        return DRB12816_ERROR_NO_FULLRANK_MAT;
    return 0;
}



