#ifndef _H_RAINBOW_CONFIG_H_
#define _H_RAINBOW_CONFIG_H_

#define _V1 32
#define _O1 32
#define _O2 32
#define _HASH_LEN 32

#define _GFSIZE 16

#define _V2 (_V1 + _O1)

#define _V1_BYTE (_V1 / 2)
#define _V2_BYTE (_V2 / 2)
#define _O1_BYTE (_O1 / 2)
#define _O2_BYTE (_O2 / 2)

#define _PUB_N (_V1 + _O1 + _O2)
#define _PUB_M (_O1 + _O2)
#define _SEC_N (_PUB_N)
#define _PUB_N_BYTE (_PUB_N / 2)
#define _PUB_M_BYTE (_PUB_M / 2)

#define _SALT_BYTE 16

#define _SIGNATURE_BYTE (_PUB_N_BYTE + _SALT_BYTE)

#define TERMS_QUAD_POLY(N) (((N) * ((N) + 1) / 2) + (N) + 1)

// extra one byte for salt
//#define _PUB_KEY_LEN (TERMS_QUAD_POLY(_PUB_N) * (_PUB_M_BYTE) + 1)

#endif
