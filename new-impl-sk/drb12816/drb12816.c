#include "drb12816.h"

// This header file provides:
//      the definition for struct prng_context
//      the prototype for function prng_begin()
//      the prototype for function prng_gen()
//      the prototype for function prng_end()
//      the prototype for function sha256()
#include "../external/external.h"


#include "keys_for_wallet.h"


int drb12816_genpubchunk(unsigned char *pubchunk, const unsigned char *seed, unsigned seedlen, unsigned chunkid) {

    abcrb_secretkey_t sk;
    int err = abcrb_seed2prv( &sk, seed, seedlen );
    if( err ) return err;

    err = gen_pubkey_chunk( pubchunk, &sk, chunkid );
    return err;
}

#include "drb12816_internal.h"

int drb12816_sign(unsigned char *sig, const unsigned char *seed, unsigned seedlen, const unsigned char *dgst) {

    abcrb_secretkey_t sk;
    int err = abcrb_seed2prv( &sk, seed, seedlen );
    if( err ) return err;

    err = abcrb_sign( sig , &sk, dgst );
    return err;
}

