#include <stdalign.h>
#include <string.h>

#include "blas.h"
#include "mpkc.h"

// This header file provides:
//      the definition for struct prng_context
//      the prototype for function prng_begin()
//      the prototype for function prng_gen()
//      the prototype for function prng_end()
//      the prototype for function sha256()
#include "../external/external.h"


#include "keys_for_wallet.h"
#include "drb12816.h"



#define IDX_QTERMS_REVLEX(xi, xj) ((xj) * ((xj) + 1) / 2 + (xi))


///////////////////////////////////////////////////////

#define MAX_ATTEMPT_FULLRANK_MAT 96
#define MAX_DIMENSION_FULLRANK_MAT 96

int gf16mat_rand_fullrank_mat(struct prng_context *prng_ctx, uint8_t *mat, unsigned dim) {
    if( MAX_DIMENSION_FULLRANK_MAT < dim ) return -1;
    uint32_t buffer[MAX_DIMENSION_FULLRANK_MAT*MAX_DIMENSION_FULLRANK_MAT/8];
    uint8_t *my_mat = (uint8_t*)&buffer[0];
    unsigned size_mat = dim*dim/2;
    unsigned bytes_used = 0;
    int i = 0;
    for(;i<MAX_ATTEMPT_FULLRANK_MAT;i++) {
        prng_gen(prng_ctx, mat, size_mat );
        bytes_used += size_mat;
        memcpy(my_mat,mat,size_mat);
        if( gf16mat_gauss_elim(my_mat, dim, dim) ) break;
    }
    return (MAX_ATTEMPT_FULLRANK_MAT<=i)? -1 : (int)bytes_used;
}

int gf16mat_rand_fullrank_mat2(struct prng_context *saved_prng_state, struct prng_context *prng_ctx, uint8_t *mat, unsigned dim) {
    if( MAX_DIMENSION_FULLRANK_MAT < dim ) return -1;
    uint32_t buffer[MAX_DIMENSION_FULLRANK_MAT*MAX_DIMENSION_FULLRANK_MAT/8];
    uint8_t *my_mat = (uint8_t*)&buffer[0];
    unsigned size_mat = dim*dim/2;
    unsigned bytes_used = 0;
    int i = 0;
    for(;i<MAX_ATTEMPT_FULLRANK_MAT;i++) {
        memcpy(saved_prng_state, prng_ctx, sizeof(struct prng_context) );
        prng_gen(prng_ctx, mat, size_mat );
        bytes_used += size_mat;
        memcpy(my_mat,mat,size_mat);
        if( gf16mat_gauss_elim(my_mat, dim, dim) ) break;
    }
    return (MAX_ATTEMPT_FULLRANK_MAT<=i)? -1 : (int)bytes_used;
}


int gf16mat_rand_fullrank_invmat(struct prng_context *prng_ctx, uint8_t *mat, unsigned dim) {
    if( MAX_DIMENSION_FULLRANK_MAT < dim ) return -1;
    uint32_t buffer[MAX_DIMENSION_FULLRANK_MAT*MAX_DIMENSION_FULLRANK_MAT/8];
    uint8_t *my_mat = (uint8_t*)&buffer[0];
    uint32_t buffer2[MAX_DIMENSION_FULLRANK_MAT*MAX_DIMENSION_FULLRANK_MAT/4];
    uint8_t *temp_mat = (uint8_t*)&buffer2[0];
    unsigned size_mat = dim*dim/2;
    unsigned bytes_used = 0;
    int i = 0;
    for(;i<MAX_ATTEMPT_FULLRANK_MAT;i++) {
        prng_gen(prng_ctx, my_mat, size_mat );
        bytes_used += size_mat;
        if( gf16mat_inv(mat, my_mat, dim, temp_mat) ) break;
    }
    return (MAX_ATTEMPT_FULLRANK_MAT<=i)? -1 : (int)bytes_used;
}


unsigned extract_s_map(struct prng_context *prng_ctx, unsigned prng_idx, uint8_t *mat_s, uint8_t *vec_s, const abcrb_secretkey_t *prv) {
    while (prng_idx <= prv->prng_idx_s) {
        prng_gen(prng_ctx, mat_s, _PUB_M * _PUB_M_BYTE);
        prng_idx += _PUB_M * _PUB_M_BYTE;
    }
    prng_gen(prng_ctx, vec_s, _PUB_M_BYTE);
    prng_idx += _PUB_M_BYTE;
    return prng_idx;
}


unsigned extract_invs_map(struct prng_context *prng_ctx, unsigned prng_idx, uint8_t *mat_s, uint8_t *vec_s, const abcrb_secretkey_t *prv) {
    unsigned rr = extract_s_map(prng_ctx,prng_idx,mat_s,vec_s,prv);
    uint32_t buffer2[_PUB_M*_PUB_M/4];
    uint8_t *temp_mat = (uint8_t*)&buffer2[0];
    gf16mat_inv(mat_s, mat_s, _PUB_M, temp_mat);
    return rr;
}




unsigned extract_t_map(struct prng_context *prng_ctx, unsigned prng_idx, uint8_t *mat_t, uint8_t *vec_t, const abcrb_secretkey_t *prv) {
    while (prng_idx <= prv->prng_idx_t) {
        prng_gen(prng_ctx, mat_t, _PUB_N * _PUB_N_BYTE);
        prng_idx += _PUB_N * _PUB_N_BYTE;
    }
    prng_gen(prng_ctx, vec_t, _PUB_N_BYTE);
    prng_idx += _PUB_N_BYTE;
    return prng_idx;
}

unsigned extract_invt_map(struct prng_context *prng_ctx, unsigned prng_idx, uint8_t *mat_t, uint8_t *vec_t, const abcrb_secretkey_t *prv) {
    unsigned rr = extract_t_map(prng_ctx,prng_idx,mat_t,vec_t,prv);
    uint32_t buffer2[_PUB_N*_PUB_N/4];
    uint8_t *temp_mat = (uint8_t*)&buffer2[0];
    gf16mat_inv(mat_t, mat_t, _PUB_N, temp_mat);
    return rr;
}


///////////////////////////////////////////////////////////////////////////////////////////////



void _abcrb_expand_secretkey(ns_rainbow_key *sk, const abcrb_secretkey_t *prv) {

    struct prng_context _prng_ctx;
    struct prng_context * prng_ctx = (struct prng_context *) &_prng_ctx;
    prng_begin(prng_ctx, prv->seed, SEED_LEN_FOR_KEYS );
    unsigned prng_idx = 0;

    // generate S map
    prng_idx = extract_invs_map(prng_ctx, prng_idx, sk->mat_s, sk->vec_s, prv);
    // generate S map
    prng_idx = extract_invt_map(prng_ctx, prng_idx, sk->mat_t, sk->vec_t, prv);

    // generate C map
    //prng_gen(prng_ctx, (uint8_t *) &sk->ckey, sizeof(ns_rainbow_ckey));
    uint8_t *cptr = (uint8_t *) &sk->ckey;
    prng_gen(prng_ctx, cptr, sizeof(ckey_l1));  // l1  // XXX
    cptr += sizeof(ckey_l1);

    prng_gen(prng_ctx, cptr, sizeof(ckey_l2_o_vo));  // l2-o+vo  // XXX
    cptr += sizeof(ckey_l2_o_vo);

    prng_gen(prng_ctx, cptr, sizeof(ckey_l2_vv));  // l2-vv  // XXX
    cptr += sizeof(ckey_l2_vv);
}




int abcrb_seed2prv(abcrb_secretkey_t *abcrb_prv, const uint8_t *seed, unsigned seedlen) {

    // initailize abcrb_secretkey_t
    //memcpy(abcrb_prv->seed, seed, SEED_LEN_FOR_KEYS);
    sha256(abcrb_prv->seed, seed, seedlen);
    abcrb_prv->prng_idx_s = 0;
    abcrb_prv->prng_idx_t = 0;
    abcrb_prv->prng_idx_c = 0;

    struct prng_context _prng_ctx;
    struct prng_context *prng_ctx = &_prng_ctx;
    prng_begin(prng_ctx, abcrb_prv->seed, SEED_LEN_FOR_KEYS);
    unsigned prng_idx = 0;

    uint32_t _mat_96x96[1152];   /// (96*96/2)/4
    uint8_t *mat_96x96 = (uint8_t*) &_mat_96x96;

    // S map
    int bytes = gf16mat_rand_fullrank_mat(prng_ctx, mat_96x96, _PUB_M );
    if( 0 > bytes ) return DRB12816_ERROR_NO_FULLRANK_MAT;
    prng_idx += bytes;
    abcrb_prv->prng_idx_s = prng_idx - (_PUB_M*_PUB_M_BYTE);
    prng_gen(prng_ctx, mat_96x96, _PUB_M_BYTE );  /// vector part
    prng_idx += _PUB_M_BYTE;


    // T map
    bytes = gf16mat_rand_fullrank_mat(prng_ctx, mat_96x96, _PUB_N );
    if( 0 > bytes ) return DRB12816_ERROR_NO_FULLRANK_MAT;
    prng_idx += bytes;
    abcrb_prv->prng_idx_t = prng_idx - (_PUB_N*_PUB_N_BYTE);
    prng_gen(prng_ctx, mat_96x96, _PUB_N_BYTE );  /// vector part
    prng_idx += _PUB_N_BYTE;

    /// C map
    abcrb_prv->prng_idx_c = prng_idx;

    return 0;
}



///////////////////////////////////////////////////////////////////


// description for the source of the interpolation.
typedef struct {
    uint8_t n_ele;
    uint8_t coef;
    uint8_t idx[2];
} src_interp;

static unsigned fill_out_prepare_srcs(src_interp *srcs) {
    unsigned w_idx = 0;
    // linear terms
    for (unsigned i = 0; i < _PUB_N; i++) {
        srcs[w_idx].n_ele = 1;
        srcs[w_idx].coef = 1;
        srcs[w_idx].idx[0] = i;
        w_idx++;
    }
    // quadratic terms
    for (unsigned i = 0; i < _PUB_N; i++) {
        srcs[w_idx].n_ele = 1;
        srcs[w_idx].coef = 2;
        srcs[w_idx].idx[0] = i;
        w_idx++;
    }
    // constant terms
    srcs[w_idx].n_ele = 0;
    srcs[w_idx].coef = 0;
    srcs[w_idx].idx[0] = 0;
    w_idx++;
    return w_idx;
}

static unsigned fill_out_srcs(src_interp *srcs, unsigned chunkid) {
    if ((unsigned) -1 == chunkid) {
        return fill_out_prepare_srcs(srcs);
    }

    const unsigned lines_per_chunk = DRB12816_BYTELEN_PUBCHUNK / _PUB_M_BYTE;
    unsigned first_line = chunkid * lines_per_chunk;
    unsigned w_idx = 0;
    if (TERMS_QUAD_POLY(_PUB_N) <= first_line) {
        return w_idx;
    }

    unsigned idx = 0;
    if (idx + _PUB_N <= first_line) {
        idx += _PUB_N;
    } else {
        for (unsigned i = 0; i < _PUB_N; i++) {
            if (idx < first_line) {
                idx++;
                continue;
            }
            srcs[w_idx].n_ele = 1;
            srcs[w_idx].coef = 1;
            srcs[w_idx].idx[0] = i;
            w_idx++;
            if (lines_per_chunk == w_idx) {
                return w_idx;
            }
        }
    }
    for (unsigned i = 0; i < _PUB_N; i++) {
        if (idx + (i + 1) <= first_line) {
            idx += (i + 1);
            continue;
        }
        for (unsigned j = 0; j < i; j++) {
            if (idx < first_line) {
                idx++;
                continue;
            }
            srcs[w_idx].n_ele = 2;
            srcs[w_idx].coef = 1;
            srcs[w_idx].idx[0] = i;
            srcs[w_idx].idx[1] = j;
            w_idx++;
            if (lines_per_chunk == w_idx) {
                return w_idx;
            }
        }
        if (idx < first_line) {
            idx++;
            continue;
        }
        srcs[w_idx].n_ele = 1;
        srcs[w_idx].coef = 2;
        srcs[w_idx].idx[0] = i;
        w_idx++;
        if (lines_per_chunk == w_idx) {
            return w_idx;
        }
    }
    srcs[w_idx].n_ele = 0;
    srcs[w_idx].coef = 0;
    srcs[w_idx].idx[0] = 0;
    w_idx++;

    return w_idx;
}

////////////////////////////////////////////////////////////////////////////////

static void perform_t_map(uint8_t *results_t, const uint8_t *mat_t, const uint8_t *vec_t, const src_interp *srcs, unsigned n_rows) {
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *x = (uint8_t *) (results_t + i * _PUB_N_BYTE);
        //gf256v_add(x, vec_t, _PUB_N_BYTE);
        memcpy(x, vec_t, _PUB_N_BYTE);
        // perform the matrix-vector multiplication
        for (unsigned j = 0; j < srcs[i].n_ele; j++) {
            if (1 == srcs[i].coef) {
                unsigned idx = srcs[i].idx[j];
                gf256v_add(x, mat_t + idx * _PUB_N_BYTE, _PUB_N_BYTE);
            } else {
                unsigned idx = srcs[i].idx[j];
                gf16v_madd(x, mat_t + idx * _PUB_N_BYTE, srcs[i].coef, _PUB_N_BYTE);
            }
        }
    }
}

static void my_gen_l1_mat(uint8_t *mat, const ckey_l1 *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O1; i++) {
        gf16mat_prod(mat + i * _O1_BYTE, k->l1_vo[i], _O1_BYTE, _V1, v);
        gf256v_add(mat + i * _O1_BYTE, k->l1_o + i * _O1_BYTE, _O1_BYTE);
    }
}

static void my_gen_l2_mat(uint8_t *mat, const ckey_l2_o_vo *k, const uint8_t *v) {
    for (unsigned i = 0; i < _O2; i++) {
        gf16mat_prod(mat + i * _O2_BYTE, k->l2_vo[i], _O2_BYTE, _V2, v);
        gf256v_add(mat + i * _O2_BYTE, k->l2_o + i * _O2_BYTE, _O2_BYTE);
    }
}


// private key, chunk id => a chunk of public key (only 4096 bytes)
// no external dependencies
// takes at most 10 MCU seconds (about 7 MCU minutes for full public key)
static int _abcrb_prv2pubchunk(uint8_t *pubchunk, const abcrb_secretkey_t *prv, unsigned chunkid) {

    struct prng_context _prng_ctx;
    struct prng_context *prng_ctx = &_prng_ctx;
    prng_begin(prng_ctx, prv->seed, SEED_LEN_FOR_KEYS);
    unsigned prng_idx = 0;

    //const unsigned max_eles = (RAINBOW_LEN_PUBCHUNK) / (_PUB_M_BYTE);  /// 128
    const unsigned max_eles = 200;  // for chunk -1
    // results of the T map
    alignas(4) uint8_t res_t[max_eles*_PUB_N_BYTE];

    // generating the T map
    alignas(4) uint8_t vec_t[_PUB_N_BYTE];
    alignas(4) uint8_t mat_t[_PUB_N_BYTE*_PUB_N];
    prng_idx = extract_s_map(prng_ctx, prng_idx, mat_t, vec_t, prv);  // can not temporarily store the S map.
    prng_idx = extract_t_map(prng_ctx, prng_idx, mat_t, vec_t, prv);

    // perform the T map
    src_interp srcs[max_eles];
    unsigned n_rows = fill_out_srcs(srcs, chunkid);
    if (0 == n_rows) {
        prng_end(prng_ctx);
        return DRB12816_ERROR_BAD_INPUT;  // out of correct chunkids ??
    }
    perform_t_map(res_t, mat_t, vec_t, srcs, n_rows);

    // central map
    /// presuming: sizeof( ckey_l2_vv ) > sizeof( ckey_l2_o_vo )  > sizeof( ckey_l1 ) 
    ckey_l2_vv sto_ckey;

    // fill out the ckey
    uint8_t *cptr = (uint8_t*) &sto_ckey;
    // perform the c-map:  L1
    prng_gen(prng_ctx, cptr, sizeof(ckey_l1));  // l1
    ckey_l1 *k_l1 = (ckey_l1 *) cptr;
    alignas(4) uint8_t mat1[_O1*_O1_BYTE];
    uint8_t *temp = mat_t;    /// resue stack
    // results of the central map are temporarily stored in the output ``pubchunk''
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *a = res_t + i * _PUB_N_BYTE;
        uint8_t *r = pubchunk + i * _PUB_M_BYTE;
        my_gen_l1_mat(mat1, k_l1, a);
        gf16rowmat_prod(r, mat1, _O1, _O1_BYTE, a + _V1_BYTE);
        gf16mpkc_mq_eval_n_m(temp, k_l1->l1_vv, a, _V1, _O1);
        gf256v_add(r, temp, _O1_BYTE);
    }

    // perfrom the c-map: L2-o+vo
    prng_gen(prng_ctx, cptr, sizeof(ckey_l2_o_vo));  // l2-o+vo  // XXX
    ckey_l2_o_vo *k_l2_o_vo = (ckey_l2_o_vo *) cptr;
    uint8_t *mat2 = mat1;   /// reuse stack; it is possible since _O1 = _O2
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *a = res_t + i * _PUB_N_BYTE;
        uint8_t *r = pubchunk + i * _PUB_M_BYTE + _O1_BYTE;
        my_gen_l2_mat(mat2, k_l2_o_vo, a);
        gf16rowmat_prod(r, mat2, _O2, _O2_BYTE, a + _V2_BYTE);
    }

    // perfrom the c-map: L2-vv
    prng_gen(prng_ctx, cptr, sizeof(ckey_l2_vv));  // l2-vv  // XXX
    ckey_l2_vv *k_l2_vv = (ckey_l2_vv *) cptr;
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *a = res_t + i * _PUB_N_BYTE;
        uint8_t *r = pubchunk + i * _PUB_M_BYTE + _O1_BYTE;
        gf16mpkc_mq_eval_n_m(temp, k_l2_vv->l2_vv, a, _V2, _O2);
        gf256v_add(r, temp, _O2_BYTE);
    }

    // rewind prng
    prng_begin(prng_ctx, prv->seed, SEED_LEN_FOR_KEYS);
    prng_idx = 0;
    // generating the S map
    uint8_t *vec_s = vec_t;
    uint8_t *mat_s = mat_t;
    prng_idx = extract_s_map(prng_ctx, prng_idx, mat_s, vec_s, prv);

    // perform the S map
    uint8_t temp_y[_PUB_M_BYTE];
    for (unsigned i = 0; i < n_rows; i++) {
        uint8_t *z = pubchunk + i * _PUB_M_BYTE;
        memcpy(temp_y, z, _PUB_M_BYTE);
        gf16mat_prod(z, mat_s, _PUB_M_BYTE, _PUB_M, temp_y);
        gf256v_add(z, vec_s, _PUB_M_BYTE);
    }

    prng_end(prng_ctx);
    return 0;
}





///////////////////////////////////////////////////////////////////////////////


typedef struct {
    alignas(4) uint8_t linear_terms[_PUB_N][_PUB_M_BYTE];
    alignas(4) uint8_t quad_terms[_PUB_N][_PUB_M_BYTE];
    alignas(4) uint8_t const_terms[_PUB_M_BYTE];
} pubkey_part_t;


static int gen_pubkey_1st_part( pubkey_part_t * pk, const abcrb_secretkey_t *prv ) {

    _abcrb_prv2pubchunk( (uint8_t*) pk, prv , -1 );
    pubkey_part_t *pubchunk_ctx = pk;   /// use the same storage

    alignas(4) uint8_t tmp_r0[_PUB_M_BYTE] = {0};
    alignas(4) uint8_t tmp_r1[_PUB_M_BYTE] = {0};
    alignas(4) uint8_t tmp_r2[_PUB_M_BYTE] = {0};
    uint8_t gf16_2x2 = gf16_mul(2, 2);

    for (unsigned i = 0; i < _PUB_N; i++) {
        // quad_poly(tmp_r0, key, tmp);  // v + v^2
        memcpy(tmp_r0, pubchunk_ctx->linear_terms[i], _PUB_M_BYTE);
        gf256v_add(tmp_r0, pubchunk_ctx->const_terms, _PUB_M_BYTE);

        memcpy(tmp_r2, tmp_r0, _PUB_M_BYTE);
        gf16v_mul_scalar(tmp_r0, gf16_2x2 , _PUB_M_BYTE);  // 3v + 3v^2

        // quad_poly(tmp_r1, key, tmp);  // 2v + 3v^2
        memcpy(tmp_r1, pubchunk_ctx->quad_terms[i], _PUB_M_BYTE);
        gf256v_add(tmp_r1, pubchunk_ctx->const_terms, _PUB_M_BYTE);

        gf256v_add(tmp_r0, tmp_r1, _PUB_M_BYTE);  // v
        gf256v_add(tmp_r2, tmp_r0, _PUB_M_BYTE);  // v^2
        memcpy(pubchunk_ctx->linear_terms[i], tmp_r0, _PUB_M_BYTE);
        memcpy(pubchunk_ctx->quad_terms[i], tmp_r2, _PUB_M_BYTE);
    }

    return 0;
}




int gen_pubkey_chunk(uint8_t *pubchunk, const abcrb_secretkey_t *prv, int chunkid) {

    pubkey_part_t _pk_1st;
    pubkey_part_t *pk_1st = &_pk_1st;
    gen_pubkey_1st_part( pk_1st , prv );

    int err = _abcrb_prv2pubchunk(pubchunk, prv, chunkid);
    if( err ) return err;

    src_interp srcs[(DRB12816_BYTELEN_PUBCHUNK) / (_PUB_M_BYTE)];     /// 128
    unsigned n_rows = fill_out_srcs(srcs, chunkid);

    for (unsigned i = 0; i < n_rows; i++) {
        if (srcs[i].n_ele == 1 && srcs[i].coef == 1) {
            memcpy(pubchunk + _PUB_M_BYTE * i, pk_1st->linear_terms[srcs[i].idx[0]], _PUB_M_BYTE);
        } else if (srcs[i].n_ele == 1 && srcs[i].coef == 2) {
            memcpy(pubchunk + _PUB_M_BYTE * i, pk_1st->quad_terms[srcs[i].idx[0]], _PUB_M_BYTE);
        } else if (srcs[i].n_ele == 0) {
            memcpy(pubchunk + _PUB_M_BYTE * i, pk_1st->const_terms, _PUB_M_BYTE);
        } else {
            unsigned ii = srcs[i].idx[0];
            unsigned jj = srcs[i].idx[1];
            // quad_poly(tmp_r0, key, tmp);  // v1 + v1^2 + v2 + v2^2 + v1v2
            uint8_t * dest = pubchunk + _PUB_M_BYTE * i;
            gf256v_add(dest, pk_1st->const_terms, _PUB_M_BYTE);
            gf256v_add(dest, pk_1st->linear_terms[ii], _PUB_M_BYTE);
            gf256v_add(dest, pk_1st->linear_terms[jj], _PUB_M_BYTE);
            gf256v_add(dest, pk_1st->quad_terms[ii], _PUB_M_BYTE);
            gf256v_add(dest, pk_1st->quad_terms[jj], _PUB_M_BYTE);
        }
    }

    // last byte "0x10" in the publick key
    if (37 == chunkid) {
        pubchunk[_PUB_M_BYTE * n_rows] = 0x10;
    }

    return 0;
}
