import binascii
import drbg

# sage

### globle config ###

gf = GF(16)
n=96; m=64; v1 = 32; v2=64; o1=32; o2=32;
len_salt = 32
len_hash = 64



def uint4_to_gf( a ):
  log_tab = [ -42, 0, 5, 10, 1, 4, 2, 8, 6, 13, 9, 7, 11, 12, 3, 14 ]
  if 0 == a : return gf(0)
  return gf.0^log_tab[a]

def gf_to_uint4( a ):
  log_val = int( a._log_repr() );
  exp_tab = [ 0x1,0x4,0x6,0xe,0x5,0x2,0x8,0xb,0x7,0xa,0x3,0xc,0xd,0x9,0xf,0x1 ]
  if 0 == log_val : return 0
  return exp_tab[log_val];

def uint8_to_uint4( al ):
  r = []
  for i in range(len(al)):
    il = al[i]%16
    ih = al[i]>>4
    r.append(il)
    r.append(ih)
  return r

def uint4_to_uint8( al ):
  r = []
  for i in range(0,len(al),2):
    il = al[i]
    ih = al[i+1]
    r.append( il + (ih<<4) )
  return r


def gfs_to_bytes( inp ):
  import struct
  inp_u4 = map( gf_to_uint4 , inp )
  inp_u8 = uint4_to_uint8( inp_u4 )
  return struct.pack( 'B'*len(inp_u8) , *inp_u8 )

def bytes_to_gfs( inp ):
  import struct
  inp_4 = uint8_to_uint4( struct.unpack( 'B'*len(inp) , inp ) )
  return map(  uint4_to_gf , inp_4 )

### hash

def sha256(msg):
  import hashlib;
  return hashlib.sha256(msg).digest()

def gf_hash( m , inp ):
  hr = sha256( gfs_to_bytes(inp) )
  return vector( bytes_to_gfs( hr )[:m] )





### prng related functions

def prng_set_gfhash( seed ):
  return [ gf_hash( len_hash , seed ) , 0 ]

def prng_gen_gfhash( prng_state , length ):
  ret = []
  st = copy( prng_state )
  for i in range(length):
    if( len_hash == st[1] ) : st = prng_set( st[0] )
    ret.append( st[0][st[1]] )
    st[1] = st[1] + 1
  return ret, st

"""
New prng

p = prng_set2(b'test seed'); x, p = prng_gen2(p, 1234); y, p = prng_gen2(p, 4321)
p2 = prng_set2(b'test seed'); z, p2 = prng_gen2(p2, 5555)

assert x + y == z
"""

def prng_set2(seed_bytes):
    return [drbg.CTRDRBG('aes256', seed_bytes), '']

def prng_gen2(prng_state, length):
    ret = []
    st = copy(prng_state)
    while length > len(st[1]):
        st[1] += st[0].generate(1024)
    ret = st[1][:length]
    st[1] = st[1][length:]
    return ret, st


def adapter_prng_set2( gf_seed ):
  return prng_set2( gfs_to_bytes(gf_seed) )

def adapter_prng_gen2( prng_state , length ):
  assert 0 == (length % 2 )
  len_byte = length // 2
  r_list , r_state = prng_gen2( prng_state , len_byte )
  return bytes_to_gfs(r_list), r_state

###  prng

def prng_set( gf_seed ):
  #return prng_set_gfhash( gf_seed )
  return adapter_prng_set2( gf_seed )


def prng_gen( prng_state , length ):
  #return prng_gen_gfhash( prng_state , length )
  return adapter_prng_gen2( prng_state , length )



### quadratic polynomials

def quad_poly_eval( polys , w ):
  n = len( list(w))
  m = len(polys.columns())
  vars = list(w)
  for i in range(n):
    vars = vars + list( w[i]*w )[:i+1]
  vars.append( gf(1) )
  return vector(vars)*polys


def idx_quad_term( n , i , j ):
  assert( i < n )
  assert( j < n )
  assert( i <= j )
  return n + j*(j+1)//2 + i

def terms_quad_poly( n ):
  return n+ n*(n+1)//2 + 1






### key related functions

class Seckey:
  Mt = MatrixSpace( gf , n , n )
  Vt = VectorSpace( gf , n )
  Ms = MatrixSpace( gf , m , m )
  Vs = VectorSpace( gf , m )

  L1O = MatrixSpace( gf , o1 , o1 )
  L1VO = MatrixSpace( gf , v1 , o1 )
  L1VV = MatrixSpace( gf , terms_quad_poly(v1) , o1 )

  L2O = MatrixSpace( gf , o2 , o2 )
  L2VO = MatrixSpace( gf , v2 , o2 )
  L2VV = MatrixSpace( gf , terms_quad_poly(v2) , o2 )


  def __init__(self):
    self.mt = Seckey.Mt(0)
    self.vt = Seckey.Vt(0)
    self.ms = Seckey.Ms(0)
    self.vs = Seckey.Vs(0)

    self.l1o = Seckey.L1O(0)
    self.l1vo = [ Seckey.L1VO(0) for i in range(o1)]
    self.l1vv = Seckey.L1VV(0)

    self.l2o = Seckey.L2O(0)
    self.l2vo = [ Seckey.L2VO(0) for i in range(o2)]
    self.l2vv = Seckey.L2VV(0)



def seed_to_seckey( gf_seed):
  prng0 = prng_set( gf_seed )
  ret = Seckey()

  # S map
  tmp, prng0 = prng_gen(prng0, m*m )
  ret.ms = Seckey.Ms(tmp)
  while( ret.ms.is_singular() ):
    tmp, prng0 = prng_gen(prng0, m*m )
    ret.ms = Seckey.Ms(tmp)

  tmp, prng0 = prng_gen(prng0, m )
  ret.vs = Seckey.Vs( tmp )

  # T map
  tmp, prng0 = prng_gen(prng0, n*n )
  ret.mt = Seckey.Mt(tmp)
  while( ret.mt.is_singular() ):
    tmp, prng0 = prng_gen(prng0, n*n )
    ret.mt = Seckey.Mt(tmp)

  tmp, prng0 = prng_gen(prng0, n )
  ret.vt = Seckey.Vt( tmp )

  # C map , L1
  tmp, prng0 = prng_gen(prng0, o1*o1 )
  ret.l1o = Seckey.L1O( tmp )
  for i in range(o1):
    tmp, prng0 = prng_gen(prng0, v1*o1 )
    ret.l1vo[i] = Seckey.L1VO( tmp )
  tmp, prng0 = prng_gen(prng0, terms_quad_poly(v1)*o1 )
  ret.l1vv = Seckey.L1VV( tmp )
  
  # C map , L2
  tmp, prng0 = prng_gen(prng0, o2*o2 )
  ret.l2o = Seckey.L2O( tmp )
  for i in range(o2):
    tmp, prng0 = prng_gen(prng0, v2*o2 )
    ret.l2vo[i] = Seckey.L2VO( tmp )
  tmp, prng0 = prng_gen(prng0, terms_quad_poly(v2)*o2 )
  ret.l2vv = Seckey.L2VV( tmp )

  return ret


################################################################################
### something for central map

def gen_o_mat( vo_map , o_mat , vinegar ):
  ret = copy(o_mat)
  n_o = len(o_mat.rows())
  for i in range(n_o):
    ret[i] = ret[i] + vinegar*vo_map[i]
  return ret


def central_map( seckey , x ):
  v_l1 = x[:v1]
  o_l1 = x[v1:v1+o1]
  r1 = quad_poly_eval( seckey.l1vv , v_l1 ) + o_l1*(gen_o_mat(seckey.l1vo , seckey.l1o, v_l1).transpose())
  v_l2 = x[:v2]
  o_l2 = x[v2:v2+o2]
  r2 = quad_poly_eval( seckey.l2vv , v_l2 ) + o_l2*(gen_o_mat(seckey.l2vo , seckey.l2o, v_l2).transpose())
  return vector( list(r1) + list(r2))

def ivs_central_map( seckey , y , v_l1 ):
  omat_l1 = gen_o_mat(seckey.l1vo , seckey.l1o, v_l1)
  if omat_l1.is_singular() : return (False,vector([gf(0) for i in range(v2+o2)]))
  r1 = y[:o1]
  o_l1 = ( r1 + quad_poly_eval( seckey.l1vv , v_l1 ) )* (omat_l1.transpose())^-1

  v_l2 = vector( list(v_l1) + list(o_l1))
  omat_l2 = gen_o_mat(seckey.l2vo , seckey.l2o, v_l2)
  if omat_l2.is_singular() : return (False,vector([gf(0) for i in range(v2+o2)]))
  r2 = y[o1:o1+o2]
  o_l2 = ( r2 + quad_poly_eval( seckey.l2vv , v_l2 ) )* (omat_l2.transpose())^-1
    
  return True,vector( list(v_l2) + list(o_l2))



#####################################################################
### for generating public key

def pubmap_with_seckey( seckey , w ):
  x = w*seckey.mt + seckey.vt
  y = central_map( seckey , x )
  z = y*seckey.ms + seckey.vs
  return z


def seed_to_pubkey(seed):
  seckey = seed_to_seckey(seed)
  zero = Matrix( gf, [ gf(0) for i in range(n)] )
  constant_terms = pubmap_with_seckey( seckey , zero[0] )
  terms_1 = []
  terms_2 = []
  for i in range(n):
    w = copy(zero)
    w[0,i] = gf(1);
    xi = pubmap_with_seckey(seckey,w[0]) - constant_terms
    w[0,i] = gf.0;
    xixi = pubmap_with_seckey(seckey,w[0]) - constant_terms
    xixi_2 = ( xixi - xi*gf.0 )*( gf.0^2 - gf.0 )^-1
    terms_1.append( xi - xixi_2 )
    terms_2.append( xixi_2 )
  ret = copy( terms_1 )
  for i in range(n):
    for j in range(i):
      w = copy(zero)
      w[0,i] = gf(1)
      w[0,j] = gf(1)
      yij = pubmap_with_seckey(seckey,w[0]) - constant_terms - terms_1[i] - terms_1[j] - terms_2[i] - terms_2[j]
      ret.append(yij)
    ret.append(terms_2[i])
  ret.append(constant_terms)
  return Matrix(ret)



def rainbow_gen_pubkey( keyseed ):
  gf_seed = gf_hash( len_hash , bytes_to_gfs( keyseed ) )
  pk = seed_to_pubkey(gf_seed)
  l_mat = list( pk )
  l_mat2 = reduce( lambda x,y: list(x)+list(y) , l_mat )
  return gfs_to_bytes( l_mat2 )


#########################################################################


def _do_rainbow_verify( pubkey , dig , sig , salt ):
  ck0 = gf_hash( m , list(digest) + list(salt) )
  ck1 = quad_poly_eval( pubkey , sig )
  return ck0 == ck1


def rainbow_verify( pubkey , dig , sig ):
  gf_pk = MatrixSpace( gf , terms_quad_poly( n ) , m )( bytes_to_gfs(pubkey) )
  gf_dig = bytes_to_gfs( dig )
  gf_sig = bytes_to_gfs( sig[:(n//2)] )
  gf_salt = bytes_to_gfs( sig[(n//2):] )
  return _do_rainbow_verify( gf_pk , vector(gf_dig) , vector(gf_sig) , vector(gf_salt) )


def _do_rainbow_sign( sec_seed , digest ):
  seckey = seed_to_seckey(sec_seed)
  seed = gf_hash( len_hash , list(sec_seed) + list(digest) )
  prng_state = prng_set(seed)

  ### roll vinegar
  r_rng, prng_state = prng_gen( prng_state , v1 )
  v_l1 = vector(r_rng)
  mo1 = gen_o_mat(seckey.l1vo , seckey.l1o, v_l1 )
  while( mo1.is_singular() ):
    r_rng, prng_state = prng_gen( prng_state , v1 )
    v_l1 = vector( r_rng )
    mo1 = gen_o_mat(seckey.l1vo , seckey.l1o, v_l1 )

  ### main work start
  r = False
  time = 0
  while not r :
    # roll salt
    salt , prng_state = prng_gen( prng_state , len_salt )
    digest_salt = vector( list(digest) + list(salt) )
    z = gf_hash( m , digest_salt )

    y = ( z - seckey.vs )* seckey.ms^-1
    r,x = ivs_central_map( seckey , y , v_l1 )
    w  = (x - seckey.vt)*seckey.mt^-1
    
    time = time + 1
    if time >= 128 : break
  return w,vector(salt),r
    
def rainbow_sign( keyseed , digest ):
  gf_seed = gf_hash( len_hash , bytes_to_gfs( keyseed ) )
  gf_dig = bytes_to_gfs( digest )
  gf_sig , gf_salt , r = _do_rainbow_sign( gf_seed , gf_dig )
  return r , gfs_to_bytes( list(gf_sig) + list(gf_salt) )







##############################################
## testing

# 0: set key and digest

key = binascii.unhexlify( 'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC501A00000000000020080000' )
gf_seed = gf_hash( len_hash , bytes_to_gfs( key ) )
print "seed:", binascii.hexlify( gfs_to_bytes(gf_seed) )   ###########################

# TEST 1 from new-impl-sk/test.c
raw_dig = binascii.unhexlify('e75b10130b0f1d4d81d7ef0d02c3d8a4f83c2a48ef34cdf16f703b1d2a21f422') #b'\x00\x01\x02\x03'*8
dig = bytes_to_gfs( raw_dig )
print "digest:", dig

sk = seed_to_seckey( gf_seed )



# TEST 0 self sign/verify test
# private sign/verify functions
sig, salt, r = _do_rainbow_sign( gf_seed , dig )
print "sig,salt,r:", binascii.hexlify( gfs_to_bytes(sig)) , binascii.hexlify( gfs_to_bytes(salt)) , r

z0 = pubmap_with_seckey( sk , sig )
print "cp1:", z0
z1 = gf_hash( m , list(dig) + list(salt) )
print "cp0:", z1
print z0 == z1

# comparing results of private functions to the public signing function.
r, byte_sig = rainbow_sign( key , raw_dig )
print r
sig2 = vector(bytes_to_gfs( byte_sig[:(n//2)] ) )
salt2 = vector(bytes_to_gfs( byte_sig[(n//2):] ) )
print "sig eq?", sig == sig2
print "salt eq?", salt == salt2
print "full signature:", binascii.hexlify(byte_sig)

# TEST 1 from new-impl-sk/test.c
# comparing the signature to the result of C code
# assert binascii.hexlify(byte_sig) == '03b81c34712a28578166a3ddc09814b0cc8121130b6566727dcbdf8a855d905ba6a083ac3e8bbfb0b4f4ecb106c375aa26d1bc46842f92e152a4ab64d8dede56'

# TEST 2 public keys
# VERY  slooooooooooooooooooooooow
# pk = rainbow_gen_pubkey( key )
# print "pk[:16]:" , binascii.hexlify( pk[:16] )

