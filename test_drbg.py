import binascii
import drbg
from copy import copy



def prng_set2(seed_bytes):
    return [drbg.CTRDRBG('aes256', seed_bytes), '']

def prng_gen2(prng_state, length):
    ret = []
    st = copy(prng_state)
    while length > len(st[1]):
        st[1] += st[0].generate(1024)
    ret = st[1][:length]
    st[1] = st[1][length:]   
    return ret, st





prng_st = prng_set2( b'\x00\x01' *16 )
a , prng_st = prng_gen2( prng_st , 10 )
print( "a[10]: " + binascii.hexlify(a) )






